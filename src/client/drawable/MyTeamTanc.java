package client.drawable;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class MyTeamTanc extends DrawableObject {
    protected int direction;
    protected BufferedImage imagine_nord;
    protected BufferedImage imagine_sud;
    protected BufferedImage imagine_est;
    protected BufferedImage imagine_vest;

    public MyTeamTanc() {
        super();
    }

    /**
     * @return the direction
     */
    public int getDirection() {
        return direction;
    }

    /**
     * @param direction the direction to set
     */
    public void setDirection(int direction) {
        this.direction = direction;
        if (direction == 0) {
            this.imagine_atasata = this.imagine_nord;
            return;
        }
        if (direction == 1) {
            this.imagine_atasata = this.imagine_sud;
            return;
        }
        if (direction == 2) {
            this.imagine_atasata = this.imagine_est;
            return;
        }
        if (direction == 3) {
            this.imagine_atasata = this.imagine_vest;
            return;
        }
    }

    public void loadImages() {
        try {
            this.imagine_est = ImageIO.read(getClass().getResource("/img/TeamTankEast.png"));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            System.out.println("Nu s-a incarcat imagine est");
            e.printStackTrace();
        }
        try {
            this.imagine_vest = ImageIO.read(getClass().getResource("/img/TeamTankWest.png"));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            System.out.println("Nu s-a incarcat imagine vest");
            e.printStackTrace();
        }
        try {
            this.imagine_nord = ImageIO.read(getClass().getResource("/img/TeamTankNorth.png"));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            System.out.println("Nu s-a incarcat imagine nord");
            e.printStackTrace();
        }
        try {
            this.imagine_sud = ImageIO.read(getClass().getResource("/img/TeamTankSouth.png"));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            System.out.println("Nu s-a incarcat imagine sud");
            e.printStackTrace();
        }
    }

    public void init() {
        this.loadImages();
        this.imagine_atasata = this.imagine_nord;
        this.direction = 0;
    }
}
