package client.drawable;

import javax.imageio.ImageIO;
import java.io.IOException;

/*
 * Este folosita pentru reprezentarea logica si grafica a elementelor de tip projectile care implementeaza
 *  doar metoda loadImages(), prin care se incarca imaginea 
 * folosita pentru reprentarea proiectilelor pe harta, iar constructorul apeleaza in interior metoda aceasta.
 */
public class Projectile extends DrawableObject {
    public Projectile() {
        super();
        loadFile();

    }

    public void loadFile() {
        try {
            this.imagine_atasata = ImageIO.read(getClass().getResource("/img/Bomb.png"));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            System.err.println("Nu s-a deschis imagine bomba");
        }
    }

}
