package client.drawable;

import javax.imageio.ImageIO;
import java.io.IOException;
/*
 * Este folosita pentru reprezentarea logica si grafica a elementelor de tip Walls care implementeaza doar metoda loadImages(), prin care se incarca imaginea folosita pentru reprezentarea zidurilor pe harta.
 *  Imaginea atasata pentru zid are dimensiunea 10x10 si este de tip png.
 */


public class Walls extends DrawableObject {
    public Walls() {
        super();
        loadFile();

    }

    public void loadFile() {
        try {
            this.imagine_atasata = ImageIO.read(getClass().getResource("/img/BrickWalElement.png"));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            System.err.println("Nu s-a deschis imagine bomba");
        }
    }

}
