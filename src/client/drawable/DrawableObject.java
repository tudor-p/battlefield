package client.drawable;

import java.awt.*;
import java.awt.image.BufferedImage;

/*Clasa principala din acest pachet este Clasa DrawableObject care va reprezenta varful ierarhiei de clase de desenare.
Atributele acestei clase sunt pozition care va fi folosita pentru a pastra pozitia elementuli desenabil pe harta,imagine_atasata care reprezinta o imagine in format jpeg sau png care va fi desenata pe suprafata de desenare, si ID-ul care este un element de identificare unic cu rol in logica jocului:structurile de date folosite pt reprezentarea acestor elemente de grafica utilizeaza acest identificator numeric pt ordonarea datelor astfel incat accesul sa fie cat mai rapid.
Metodele acestei clase sunt cele de tip getter si setter folosite pt accesul la elemente, dar au fost folosite si metodele init(), drawObject() si setDirection(int direction) care vor fi suprascrise in clasele derivatele  pentru o implementare polimorfica unitara a elementelor grafice. .
Din aceasta clasa au fost derivate clasele Tanc, MyTeamTanc,EnemyTeamTanc, Projectile si Walls.
*/
public abstract class DrawableObject {
    //atribute
    PointClass pozition;
    BufferedImage imagine_atasata;
    int ID;

    public DrawableObject() {
        this.pozition = new PointClass();
    }

    //metode getter si setter;
    public int getID() {
        return ID;
    }

    public void setID(int id) {
        ID = id;
    }

    public PointClass getPozition() {
        return pozition;
    }

    public void setPozition(PointClass pozition) {
        this.pozition = pozition;
    }

    public void setPozition(int x_pozition, int y_pozition) {
        this.pozition.setX_pozition(x_pozition);
        this.pozition.setY_pozition(y_pozition);
    }

    //metoda de desenare
    public void drawObject(Graphics g) {
        g.drawImage(this.imagine_atasata, this.pozition.getX_pozition() * 10, this.pozition.getY_pozition() * 10, null, null);
    }

    public void init() {

    }

    public void setDirection(int direction) {

    }


}
