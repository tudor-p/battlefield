package client.drawable;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;

/*
 * Este folosita pentru reprezentarea logica si grafica a elementelor de harta de tip tanc.
Atributele acestei clase , in afara celor din superclasa pe care o deriveaza sunt: direction  de tip intreg si 4 imagini de tip dublu bufferate care vor citi 4 imagini de tip png care reprezinta imaginea tancului in fiecare din cele patru orientari:Nord, Sud, Est,Vest. Directia va fi folosita pentru a selecta imaginea care va fi desenata pe suprafata de desenare.
Metodele acestei clase sunt gettere si settere la care se adauga metoda init care o suprascrie pe cea din superclasa si metoda loadImages(), care este folosita pentru a incarca in structura interna un fisier extern de tip png.
Clasele EnemyTeamTanc si MyTeamTanc au aceeasi structura ca si clasa Tanc dar folosesc alte fisiere externe pt reprezentarea imaginilor atasate tancurilor.
 */
public class Tanc extends DrawableObject {
    protected int direction;
    protected BufferedImage imagine_nord;
    protected BufferedImage imagine_sud;
    protected BufferedImage imagine_est;
    protected BufferedImage imagine_vest;

    public Tanc() {
        super();
    }

    /**
     * @return the direction
     */
    public int getDirection() {
        return direction;
    }

    /**
     * @param direction the direction to set
     */
    public void setDirection(int direction) {
        this.direction = direction;
        if (direction == 0) {
            this.imagine_atasata = this.imagine_nord;
            return;
        }
        if (direction == 1) {
            this.imagine_atasata = this.imagine_sud;
            return;
        }
        if (direction == 2) {
            this.imagine_atasata = this.imagine_est;
            return;
        }
        if (direction == 3) {
            this.imagine_atasata = this.imagine_vest;
            return;
        }
    }

    public void loadImages() {
        try {
            this.imagine_est = ImageIO.read(getClass().getResource("/img/YellowTankEast.png"));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            System.out.println("Nu s-a incarcat imagine est");
            e.printStackTrace();
        }
        try {
            this.imagine_vest = ImageIO.read(getClass().getResource("/img/YellowTankWest.png"));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            System.out.println("Nu s-a incarcat imagine vest");
            e.printStackTrace();
        }
        try {
            this.imagine_nord = ImageIO.read(getClass().getResource("/img/YellowTankNorth.png"));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            System.out.println("Nu s-a incarcat imagine nord");
            e.printStackTrace();
        }
        try {
            this.imagine_sud = ImageIO.read(getClass().getResource("/img/YellowTankSouth.png"));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            System.out.println("Nu s-a incarcat imagine sud");
            e.printStackTrace();
        }
    }

    public void init() {
        this.loadImages();
        this.imagine_atasata = this.imagine_nord;
        this.direction = 0;
    }


}
