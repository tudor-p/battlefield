package client.drawable;

/*
 * Desi Java contine o clasa Point pentru a retine coordonatele unui punct am decis sa imi implementez propria clasa PointClass mai simpla .
 * Metodele acestei clase sunt doar de tip getter si setter.
 */
public class PointClass {
    //clasa va fi utilizata pentru retinerea coordonatelor elementelor de pe harta


    //atribute
    int x_pozition;
    int y_pozition;


    //constructori
    public PointClass(int x_pozition, int y_pozition) {
        super();
        this.x_pozition = x_pozition;
        this.y_pozition = y_pozition;
    }

    public PointClass() {
        this(0, 0);
    }

    //methode
    public int getX_pozition() {
        return x_pozition;
    }

    public void setX_pozition(int x_pozition) {
        this.x_pozition = x_pozition;
    }

    public int getY_pozition() {
        return y_pozition;
    }

    public void setY_pozition(int y_pozition) {
        this.y_pozition = y_pozition;
    }


}
