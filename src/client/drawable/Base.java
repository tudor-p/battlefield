package client.drawable;

import javax.imageio.ImageIO;
import java.io.IOException;

/*
 * Este clasa folosita pentru reprezentarea bazelor celor 2 echipe.La fel ca clasele Projectile si Walls implementeaza metoda loadImages()
 *  care incarca imaginea folosita pentru implementarea reprezentarii grafice a bazelor celor 2 echipe.
 */
public class Base extends DrawableObject {
    public Base() {
        super();
        loadFile();

    }

    public void loadFile() {
        try {
            this.imagine_atasata = ImageIO.read(getClass().getResource("/img/Emblem.png"));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            System.err.println("Nu s-a deschis imagine bomba");
        }
    }

}
