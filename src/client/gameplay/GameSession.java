package client.gameplay;

import client.drawable.Base;
import client.drawable.Projectile;
import client.drawable.Walls;
import client.network.ClientThread;

import java.util.ArrayList;
import java.util.TreeMap;

/*
 * Clasa GameSession
	Aceasta clasa se ocupa de retinerea datelor care descriu datele ce asigura managementul logic si grafic al jocului.
Atributele acestei clase:
	protected Interpretor decoder;
	protected boolean now_playing=false;
	protected ClientThread thread_comunicatii;
	protected int game_joined;
	protected TreeMap<Integer,Player> game_players;
	protected TreeMap<Integer,Projectile> game_projectiles;
	protected TreeMap<Integer,Walls> game_walls;
	protected ArrayList<String> game_messages;
	protected ArrayList<Base> bases;
	protected TreeMap<Integer,Player> lobby_players;
	
	//elemente de grafica
	protected ChenarDesenare suprafata_desenare;
	
	//userul curent
	protected Player my_player;//desemneaza jucatorul meu
	
	//colectia de detinatori de jocuri;
	protected TreeMap<Integer,GameOwners> game_owners;
Interpretor decoder este o referinta catre un obiect reprezentat al clasei Interpretor care are rolul de a interpreta mesajele venite de la server.
now_playing este un atribut care arata daca un jucatorul este momentan intr-un joc sau nu;
int game_joined este un atribut care retine jocul pe care jucatorul l-a ales pentru a juca.
TreeMap<Integer,Player> game_players este un red_black tree in care se retin referinte catre jucatorii din jocul curent.
TreeMap<Integer,Player> lobby_players este o structura de date in care se retin totalitatea jucatorilor existenti pe server la un moment dat.
TreeMap <Integer,Projectile> este un red_black tree care retine proiectilele care se afla la un moment dat pe harta. Ordonarea elementelor se face dupa ID-ul atasat elementului de tip grafic.
TreeMap<Integer,Walls> este un red_black tree care se ocupa de retinerea elementelor de zid care sunt pe harta in joc.
ArrayList <String > messages este o colectie de diferite mesaje ce vor fi desenate pe suprafata de desenare (exemplu knd un jucator a fost omorat se afiseaza looozer);
Arraylist <Base> retine cele 2 baze din joc
Player my_player este o referinta catre jucatorul propriu avand rol in o mai usoara manevrare a unor date in cadrul algoritmilor ce se aplica asupra structurilor de date.
	Metodele acestei clase sunt de tip get , set sau deoarece avem colectii de date avem metode de tip adaugare de elemente.
 */
public class GameSession {
    protected Interpretor decoder;
    protected boolean now_playing = false;
    protected ClientThread thread_comunicatii;
    protected int game_joined;

    //colectiile de date  care se deseneaza;
    protected TreeMap<Integer, Player> game_players;
    protected TreeMap<Integer, Projectile> game_projectiles;
    protected TreeMap<Integer, Walls> game_walls;
    protected ArrayList<String> game_messages;
    protected ArrayList<Base> bases;
    //colectia persistenta de useri;
    protected TreeMap<Integer, Player> lobby_players;
    //elemente de grafica
    protected ChenarDesenare suprafata_desenare;
    //userul curent
    protected Player my_player;//desemneaza jucatorul meu
    //colectia de detinatori de jocuri;
    protected TreeMap<Integer, GameOwners> game_owners;

    //constructori
    public GameSession(ClientThread c) {
        decoder = new Interpretor(this);
        this.suprafata_desenare = new ChenarDesenare("/img/background.jpg", this);
        this.my_player = new Player();
        this.thread_comunicatii = c;
        this.game_owners = new TreeMap<Integer, GameOwners>();
        this.game_players = new TreeMap<Integer, Player>();
        this.game_projectiles = new TreeMap<Integer, Projectile>();
        this.game_walls = new TreeMap<Integer, Walls>();
        this.lobby_players = new TreeMap<Integer, Player>();
        this.game_messages = new ArrayList<String>();
        this.game_joined = -1;
        this.bases = new ArrayList<Base>();
    }

    /**
     * @return the bases
     */
    public ArrayList<Base> getBases() {
        return bases;
    }

    /**
     * @return the game_messages
     */
    public ArrayList<String> getGame_messages() {
        return game_messages;
    }

    public void addNewGameMessage(String message) {
        this.game_messages.add(message);
    }


    //	gettere si settere
    public Interpretor getDecoder() {
        return decoder;
    }

    public void setDecoder(Interpretor decoder) {
        this.decoder = decoder;
    }

    public Player getMy_player() {
        return my_player;
    }

    public void setMy_player(Player my_player) {
        this.my_player = my_player;
    }

    public ChenarDesenare getSuprafata_desenare() {
        return suprafata_desenare;
    }

    public void setSuprafata_desenare(ChenarDesenare suprafata_desenare) {
        this.suprafata_desenare = suprafata_desenare;
    }


    /**
     * @return the game_players
     */
    public TreeMap<Integer, Player> getGame_players() {
        return game_players;
    }


    /**
     * @return the game_projectiles
     */
    public TreeMap<Integer, Projectile> getGame_projectiles() {
        return game_projectiles;
    }


    /**
     * @return the game_walls
     */
    public TreeMap<Integer, Walls> getGame_walls() {
        return game_walls;
    }


    /**
     * @return the lobby_players
     */
    public TreeMap<Integer, Player> getLobby_players() {
        return lobby_players;
    }

    //adaugare de jucatori lobby
    public void adaugaLobbyPlayer(Player player) {
        this.lobby_players.put(Integer.valueOf(player.getPlayer_ID()), player);
    }

    public void adaugaGamePlayer(Player player) {
        this.game_players.put(Integer.valueOf(player.getPlayer_ID()), player);
    }

    public void adaugaWall(Walls wall) {
        this.game_walls.put(Integer.valueOf(wall.getID()), wall);
    }

    public void adaugaProjectile(Projectile proiectil) {
        this.game_projectiles.put(Integer.valueOf(proiectil.getID()), proiectil);
    }

    public void adaugaGameOwners(GameOwners owner) {
        this.game_owners.put(Integer.valueOf(owner.getId_joc()), owner);
    }


    /**
     * @return the game_owners
     */
    public TreeMap<Integer, GameOwners> getGame_owners() {
        return game_owners;
    }


    /**
     * @return the thread_comunicatii
     */
    public ClientThread getThread_comunicatii() {
        return thread_comunicatii;
    }


    /**
     * @return the game_joined
     */
    public int getGame_joined() {
        return game_joined;
    }


    /**
     * @param game_joined the game_joined to set
     */
    public void setGame_joined(int game_joined) {
        this.game_joined = game_joined;
    }


}
