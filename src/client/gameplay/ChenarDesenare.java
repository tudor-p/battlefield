package client.gameplay;

import client.drawable.Projectile;
import client.drawable.Walls;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Map;

/*
 * Clasa ChenarDesenare extinde clasa JPanel si va reprezenta containerul pe care se vor desena toate elementele grafice ale jocului.
Atributele acestei clase sunt:
	-public static final int XDIMESNION=600;
	-public static final int YDIMENSION=700;
	-protected GameSession game_to_draw;
	-protected BufferedImage background_image; 
XDIMESNION si YDIMENSION reprezinta cele 2 dimensiuni ale panelului
game_to_draw este o referinta catre clasa care contine elementele ce vor fi desenate
background_image este o imagine bufferata care va desenta ca fundal(introdusa la cereri din partea clientului)
Metodele a cestei clase sunt constructorul cu parametrul String s si GameSession g , dintre care parametrus s va reprezenta calea spre fotografia ce va fi folosita ca fundal;
Intrucat aceasta clasa extinde JPanel am suprscris metoda PaintComponent astfel incat sa pot updata elementele grafice implicit sau explicit. Aceasta metoda are rolul de a micsora la minim efectul de flickering care apare la 
desenari repetate ale elementelelor.
 */
public class ChenarDesenare extends JPanel {
    public static final int XDIMESNION = 600;
    public static final int YDIMENSION = 700;
    protected GameSession game_to_draw;
    protected BufferedImage background_image;

    //constructor
    public ChenarDesenare(String s, GameSession g) {
        this.game_to_draw = g;
        try {
            this.background_image = ImageIO.read(getClass().getResource(s));
            System.err.println("S-a incarcat poza de backgrounf");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            System.err.println("Nu s-a putut incarca imaginea de backGround");
            e.printStackTrace();
        }
    }

    public void paintComponent(Graphics g) {
        //for(this.colectie_obiecte.)
        g.drawImage(this.background_image, 0, 0, null);
        System.err.println("Lista de jucatori are nr de obiecte:" + this.game_to_draw.getGame_players().size());
        System.out.println("Lista de blocuri are :" + this.game_to_draw.getGame_walls().size());
        System.err.println("Lista de proiectile are:" + this.game_to_draw.getGame_projectiles().size());
        for (Map.Entry<Integer, Player> entry : game_to_draw.getGame_players().entrySet()) {
            entry.getValue().getTanc().drawObject(g);
        }
        //TODO grija mare
        for (Map.Entry<Integer, Projectile> entry : game_to_draw.getGame_projectiles().entrySet()) {
            entry.getValue().drawObject(g);
        }
        for (Map.Entry<Integer, Walls> entry : game_to_draw.getGame_walls().entrySet()) {
            entry.getValue().drawObject(g);
        }
        for (int i = 0; i < this.game_to_draw.getGame_messages().size(); i++) {
            g.setColor(Color.RED);
            g.setFont(new Font("Gigi", Font.BOLD, 44));
            g.drawString(this.game_to_draw.getGame_messages().get(i), 150, i * 15 + 140);

        }
        System.err.println("exista baze in numar de:  " + this.game_to_draw.getBases().size());
        for (int i = 0; i < this.game_to_draw.getBases().size(); i++) {
            this.game_to_draw.getBases().get(i).drawObject(g);
        }

    }

}
