package client.gameplay;

/*
 * Clasa GameOwners se ocupa cu retinerea datelor unui player care a creat un joc. Aceasta clasa are 3 atribute :
	-protected String nick;
		-protected int id_joc;
		-protected int id_jucator;
	nick reprezinta pseudonumele pe care si-l atribuie un jucator la intrarea pe server.
	id_jucator reprezinta un identificator numeric al jucatorului care va fi folosit pentru manipularea datelor
	id_joc reprezinta un identificator numeric dat de server la crearea unui joc nou si are rol in manipularea elementelor de tip joc
	Metodele acestei clase sunt cele de tip getter si setter cu rolul de a manipula datele retinute.

 */
public class GameOwners {
    protected String nick;
    protected int id_joc;
    protected int id_jucator;

    /**
     * @return the nick
     */
    public String getNick() {
        return nick;
    }

    /**
     * @param nick the nick to set
     */
    public void setNick(String nick) {
        this.nick = nick;
    }

    /**
     * @return the id_joc
     */
    public int getId_joc() {
        return id_joc;
    }

    /**
     * @param id_joc the id_joc to set
     */
    public void setId_joc(int id_joc) {
        this.id_joc = id_joc;
    }

    /**
     * @return the id_jucator
     */
    public int getId_jucator() {
        return id_jucator;
    }

    /**
     * @param id_jucator the id_jucator to set
     */
    public void setId_jucator(int id_jucator) {
        this.id_jucator = id_jucator;
    }

}
