package client.gameplay;

import client.network.ClientThread;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/*
 * Clasa JBattleFiledFrame
	Aceasta clasa extinde clasa JFrame si care va fi utilizata ca fereastra de afisare a elementelor grafice din joc. 
Atributele acestei clase sunt:
	protected ClientThread thread_comunicatii;
	protected ChenarDesenare chenar;
	protected boolean fired=false;
fired este o variabila care arata dak ultima tasta apasata este tasta de foc. S-a luat decizia de folosi aceasta variabila pentru a reduce fluxul de date pe retea care ar aparea in cazul tinerii apasate a tastei fire , jucatorul nu poate trage dak proiectilul tras deja nu a disparut de pe harta.
ChenarDesenare chear reprezinta chenarul pe care se deseneaza elementele grafice.
ClientThread thread_comunicatii are rolul de a retine o referinta catre threadul de comunicare cu serverul intrucat de fereastra de joc au fost agatate evenimentele de miscare si de atac ale jucatorului prin implementarea interfetei KeyListener. De asemenea s-a implementat si interfata WindowListener necesara pentru detectarea evenimentului
 de inchidere az in care jucatorul va trimite catre server un mesaj de parasire a jocului.
 */
public class JBattleFieldFrame extends JFrame implements KeyListener, WindowListener {
    protected ClientThread thread_comunicatii;
    protected ChenarDesenare chenar;
    protected boolean fired = false;

    public JBattleFieldFrame(ClientThread th, ChenarDesenare c) {
        // TODO Auto-generated constructor stub
        this.thread_comunicatii = th;
        this.setPreferredSize(new Dimension(608, 735));
        this.chenar = c;
        this.setContentPane(chenar);
        this.pack();
        this.setSize(608, 735);
        this.addWindowListener(this);
        this.addKeyListener(this);
    }

    public void keyPressed(KeyEvent arg0) {
        if (arg0.getKeyCode() == arg0.VK_UP)
            this.thread_comunicatii.sendMessage("North");
        if (arg0.getKeyCode() == arg0.VK_DOWN)
            this.thread_comunicatii.sendMessage("South");
        if (arg0.getKeyCode() == arg0.VK_RIGHT)
            this.thread_comunicatii.sendMessage("East");
        if (arg0.getKeyCode() == arg0.VK_LEFT)
            this.thread_comunicatii.sendMessage("West");
        if (arg0.getKeyCode() == arg0.VK_ESCAPE) {
            this.thread_comunicatii.sendMessage("quit");
        }
        if (arg0.getKeyCode() == arg0.VK_F && (fired == false)) {
            this.thread_comunicatii.sendMessage("fire");
            fired = true;
        }
        //arg0.consume();
        System.out.println("S-a intrceptata o apasare de tasta");

    }


    public void keyReleased(KeyEvent arg0) {
        if (arg0.getKeyCode() == arg0.VK_F) {
            this.thread_comunicatii.sendMessage("efire");
            fired = false;
        }
        System.out.println("S-a intrceptata o apasare de tasta");
        if (arg0.getKeyCode() == arg0.VK_UP)
            this.thread_comunicatii.sendMessage("stop");
        if (arg0.getKeyCode() == arg0.VK_DOWN)
            this.thread_comunicatii.sendMessage("stop");
        if (arg0.getKeyCode() == arg0.VK_RIGHT)
            this.thread_comunicatii.sendMessage("stop");
        if (arg0.getKeyCode() == arg0.VK_LEFT)
            this.thread_comunicatii.sendMessage("stop");

    }

    public void keyTyped(KeyEvent arg0) {
        // TODO Auto-generated method stub
        System.out.println("S-a intrceptata o apasare de tasta");
    }

    @Override
    public void windowActivated(WindowEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void windowClosed(WindowEvent e) {
        // TODO Auto-generated method stub
        this.setVisible(false);
        this.thread_comunicatii.getMyGameSession().decoder.decodeMessage("gameOver " + this.thread_comunicatii.getMyGameSession().game_joined);
        System.err.println("S-a inchis fereastra in winclosed");

    }

    @Override
    public void windowClosing(WindowEvent e) {
        // TODO Auto-generated method stub
        this.setVisible(false);
        //this.thread_comunicatii.getMyGameSession().decoder.decodeMessage("gameOver "+this.thread_comunicatii.getMyGameSession().game_joined);
        System.err.println("S-a inchis fereastra");
        this.thread_comunicatii.sendMessage("quit");
        this.thread_comunicatii.getLobby_frame().setVisible(true);
        this.thread_comunicatii.getMyGameSession().getBases().clear();
        this.thread_comunicatii.getMyGameSession().getGame_messages().clear();
        this.thread_comunicatii.getMyGameSession().getGame_projectiles().clear();
        this.thread_comunicatii.getMyGameSession().getGame_walls().clear();
        this.thread_comunicatii.getMyGameSession().game_players.clear();
        this.thread_comunicatii.getLobby_frame().create_button_pressed = false;
        this.thread_comunicatii.getLobby_frame().join_button_pressed = false;
        this.thread_comunicatii.getLobby_frame().create_button.setText("Create");

    }

    @Override
    public void windowDeactivated(WindowEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void windowDeiconified(WindowEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void windowIconified(WindowEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void windowOpened(WindowEvent e) {
        // TODO Auto-generated method stub

    }
}
