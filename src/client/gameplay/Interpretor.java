package client.gameplay;

import client.drawable.*;

import java.awt.*;

/*
 * Clasa Interpretor
	Aceasta clasa se ocupa de tratarea evenimentelor venite de la server. Threadul prin care se realizeaza comunicarea cu serverul paseaza mesajul sesiunii de joc care la randul ei spre metoda DecodeMesage(String message);
Aceasta metoda la randul ei cheama o alta metoda de decodificare in functie de mesajul primit.
	Ca atribut clasa poseda o referinta catre sesiunea de joc in care se gasesc striucturile de date ce vor fi modificate in urma decodificarii mesajelor primite de la server.
 */
public class Interpretor {
    protected GameSession current_game_session;

    public Interpretor(GameSession g) {
        this.current_game_session = g;
    }


    //methoda cea mai importanta a clasei;
    //in ea se va decodifica intreaga existenta a unui user;
    public void decodeMessage(String message) {
        System.out.println("Se va decodifica mesajul " + message);
        //analizarea fiecarui tip de mesaj primit

        //mesajul1 :connected id_numeric_jucator
        if (message.startsWith("connected")) {
            decodeConnected(message);
            return;
        }
        //mesajul2 :usrLst -- lista cu Useri deja conectati la server
        if (message.startsWith("usrLst")) {
            decodeUsrLst(message);
            return;
        }
        if (message.startsWith("usrNew")) {
            decodeUsrNew(message);
            return;
        }
        if (message.startsWith("gameLst")) {
            decodeGameLst(message);
            return;
        }
        if (message.startsWith("gameNew")) {
            decodeGameNew(message);
            return;
        }
        if (message.startsWith("left")) {
            decodeLeft(message);
            return;
        }
        if (message.startsWith("gone")) {
            decodeGone(message);
            return;
        }
        if (message.startsWith("nick")) {
            decodeNick(message);
            return;
        }
        if (message.startsWith("Joined")) {
            decodeJoined(message);
            return;
        }
        if (message.startsWith("ping")) {
            decodePing(message);
            return;
        }
        if (message.startsWith("gameOver")) {
            decodeGameOver(message);
            return;
        }
        if (message.startsWith("start")) {
            this.decodeStart(message);
            return;
        }
        if (message.startsWith("paint")) {
            decodePaint(message);
            return;
        }
        if (message.startsWith("erase")) {
            decodeErase(message);
            return;
        }
        if (message.startsWith("team")) {
            decodeTeam(message);
            return;
        }
    }

    //decodificarea echipei la care se aliaza jucatorul
    private void decodeTeam(String message) {
        // TODO Auto-generated method stub
        String[] comps = message.split(" ");
        int id = Integer.parseInt(comps[1]);
        if (id == this.current_game_session.getMy_player().getPlayer_ID()) {
            this.current_game_session.getMy_player().setTeam(Integer.parseInt(comps[2]));
            return;
        }
        this.current_game_session.getLobby_players().get(id).setTeam(Integer.parseInt(comps[2]));
        System.out.println("Se decodifica teamul");

    }


    private void decodeErase(String message) {
        // TODO Auto-generated method stub
        String comps[] = message.split(" ");
        if (comps[1].startsWith("MapTank")) {
            int id = Integer.parseInt(comps[2]);
            this.current_game_session.getGame_players().remove(id);
            return;
        }
        if (comps[1].startsWith("MapProjectile")) {
            int id = Integer.parseInt(comps[2]);
            this.current_game_session.getGame_projectiles().remove(id);
            return;
        }
        if (comps[1].startsWith("MapWall")) {
            int id = Integer.parseInt(comps[2]);
            this.current_game_session.getGame_walls().remove(id);
            return;
        }
        if (comps[1].startsWith("MapBase")) {
            System.err.println("SA-a sters o baza");
            int id = Integer.parseInt(comps[2]);
            for (int i = 0; i < this.current_game_session.getBases().size(); i++) {
                if (id == this.current_game_session.getBases().get(i).getID()) {
                    System.err.println("S-a gasit baza de STERSSS");
                    this.current_game_session.getBases().remove(i);
                }
            }
        }

    }


    private void decodePaint(String message) {
        // TODO Auto-generated method stub
        String[] comps = message.split(" ");
        if (comps[1].startsWith("MapTank")) {
            int id = Integer.parseInt(comps[2]);
            Player player = this.current_game_session.getGame_players().get(id);
            if (player != null) {
                System.out.println("S-a gasit tancul pt pictat");
                player.getTanc().setPozition(Integer.parseInt(comps[3]), Integer.parseInt(comps[4]));
                player.getTanc().setDirection(this.decodeDirection(comps[5]));
                return;
            }
            if (player == null) {
                int id_jucator = Integer.parseInt(comps[6]);
                if (id_jucator == this.current_game_session.getMy_player().getPlayer_ID()) {
                    System.out.println("Se adauga jucatorul meu in lista de jucatori");
                    this.current_game_session.my_player.tanc = new Tanc();
                    this.current_game_session.my_player.tanc.init();
                    this.current_game_session.my_player.setTeam(Integer.parseInt(comps[7]));
                    this.current_game_session.getMy_player().getTanc().setID(id);
                    this.current_game_session.getMy_player().getTanc().setDirection(this.decodeDirection(comps[5]));
                    this.current_game_session.getMy_player().getTanc().setPozition(Integer.parseInt(comps[3]), Integer.parseInt(comps[4]));

                    this.current_game_session.adaugaGamePlayer(this.current_game_session.getMy_player());
                    return;
                }
                player = this.current_game_session.getLobby_players().get(id_jucator);
                player.setTeam(Integer.parseInt(comps[7]));
                if (player.getTeam() == this.current_game_session.getMy_player().getTeam()) {
                    player.tanc = new MyTeamTanc();
                    player.tanc.init();
                    player.getTanc().setDirection(this.decodeDirection(comps[5]));
                    player.getTanc().setID(id);
                    player.getTanc().setPozition(Integer.parseInt(comps[3]), Integer.parseInt(comps[4]));
                    this.current_game_session.getGame_players().put(id, player);
                    return;
                } else {
                    player.tanc = new EnemyTeamTanc();
                    player.tanc.init();
                    player.getTanc().setDirection(this.decodeDirection(comps[5]));
                    player.getTanc().setID(id);
                    player.getTanc().setPozition(Integer.parseInt(comps[3]), Integer.parseInt(comps[4]));
                    this.current_game_session.getGame_players().put(id, player);
                    return;
                }
            }
            return;
        }
        if (comps[1].startsWith("MapProjectile")) {
            int id = Integer.parseInt(comps[2]);
            Projectile p = this.current_game_session.getGame_projectiles().get(id);
            if (p == null) {
                System.out.println("adaugare proiectl nou---------------");
                p = new Projectile();
                p.setID(id);
                p.setPozition(Integer.parseInt(comps[3]), Integer.parseInt(comps[4]));
                this.current_game_session.getGame_projectiles().put(id, p);
            } else {
                p.setPozition(Integer.parseInt(comps[3]), Integer.parseInt(comps[4]));
            }
            return;
        }
        if (comps[1].startsWith("MapWall")) {
            int id = Integer.parseInt(comps[2]);
            Walls w = this.current_game_session.getGame_walls().get(id);
            if (w == null) {
                w = new Walls();
                w.setID(id);
                w.setPozition(Integer.parseInt(comps[3]), Integer.parseInt(comps[4]));
                this.current_game_session.getGame_walls().put(id, w);
            }
            return;
        }
        if (comps[1].startsWith("MapBase")) {
            Base b = new Base();
            b.setPozition(Integer.parseInt(comps[3]), Integer.parseInt(comps[4]));
            b.setID(Integer.parseInt(comps[2]));
            this.current_game_session.getBases().add(b);
        }
    }

    private void decodeStart(String message) {
        // TODO Auto-generated method stub
        this.current_game_session.getThread_comunicatii().getBattle_field_frame().setVisible(true);
        this.current_game_session.getThread_comunicatii().getBattle_field_frame().setAlwaysOnTop(true);
        this.current_game_session.getThread_comunicatii().getLobby_frame().setVisible(false);


    }


    private void decodeGameOver(String message) {
        // TODO Auto-generated method stub
        String comps[] = message.split(" ");
        int id_joc = Integer.parseInt(comps[3]);
        if (id_joc == this.current_game_session.getGame_joined()) {
            //this.current_game_session.getThread_comunicatii().setClient_playing(false);
            this.current_game_session.getGame_owners().remove(id_joc);
            this.current_game_session.getThread_comunicatii().getLobby_frame().UpdateGamesBox();
            this.current_game_session.getGame_players().clear();
            this.current_game_session.getGame_projectiles().clear();
            this.current_game_session.getGame_walls().clear();
            this.current_game_session.getSuprafata_desenare().paintComponent(this.current_game_session.getSuprafata_desenare().getGraphics());
            this.current_game_session.getThread_comunicatii().getBattle_field_frame().setVisible(false);
            this.current_game_session.getGame_messages().clear();
            this.current_game_session.getThread_comunicatii().getLobby_frame().create_button_pressed = false;
            this.current_game_session.getThread_comunicatii().getLobby_frame().join_button_pressed = false;
            this.current_game_session.getThread_comunicatii().getLobby_frame().setVisible(true);
            this.current_game_session.getThread_comunicatii().getLobby_frame().create_button.setText("Create");
            this.current_game_session.getBases().clear();
            this.current_game_session.getThread_comunicatii().getLobby_frame().list_model_joined.removeAllElements();
        } else {
            this.current_game_session.getGame_owners().remove(id_joc);
            this.current_game_session.getThread_comunicatii().getLobby_frame().UpdateGamesBox();
        }
        this.current_game_session.getThread_comunicatii().getLobby_frame().list_model_joined.removeAllElements();


    }


    //primire mesaj connected
    public void decodeConnected(String message) {
        String comps[] = message.split(" ");
        //this.current_game_session.my_player= new Player();
        this.current_game_session.getMy_player().setPlayer_ID(Integer.parseInt(comps[2]));
        //this.current_game_session.adaugaLobbyPlayer(this.current_game_session.getMy_player());

    }

    //primire messaj usrLst
    public void decodeUsrLst(String message) {
        String comps[] = message.split(" ");
        Player player;
        int i = 1, id;
        while (i < comps.length) {
            id = Integer.parseInt(comps[i + 1]);
            if (id != this.current_game_session.getMy_player().getPlayer_ID()) {
                player = new Player();
                player.setNick(comps[i]);
                player.setPlayer_ID(id);
                this.current_game_session.adaugaLobbyPlayer(player);
            } else {
                //this.current_game_session.getMy_player().setNick(comps[i]);
                this.current_game_session.adaugaLobbyPlayer(this.current_game_session.getMy_player());
            }
            i += 2;

        }
        this.current_game_session.getThread_comunicatii().getLobby_frame().updateTextArea();
        this.current_game_session.getThread_comunicatii().getLobby_frame().repaint();
    }

    //primire mesaj gameLst
    public void decodeGameLst(String message) {
        String comps[] = message.split(" ");
        int i = 1;
        GameOwners owner;

        while (i < comps.length) {
            owner = new GameOwners();
            owner.setId_joc(Integer.parseInt(comps[i + 2]));
            owner.setNick(comps[i]);
            owner.setId_jucator(Integer.parseInt(comps[i + 1]));
            this.current_game_session.adaugaGameOwners(owner);
            i += 3;
        }

        System.out.println("A terminat de decodificate gameLst");
        this.current_game_session.getThread_comunicatii().getLobby_frame().UpdateGamesBox();
    }

    //primire mesaj nick -- se schimba nick-ul unui jucator
    public void decodeNick(String message) {
        String comps[] = message.split(" ");
        Integer id = Integer.parseInt(comps[2]);
        this.current_game_session.getLobby_players().get(id).setNick(comps[1]);
        this.current_game_session.getThread_comunicatii().getLobby_frame().updateTextArea();
    }

    //primire mesaj gameNew-- adaugarea unui joc nou in lista de jocuri;
    public void decodeGameNew(String message) {
        String comps[] = message.split(" ");
        GameOwners owner = new GameOwners();
        owner.setNick(comps[1]);
        owner.setId_jucator(Integer.parseInt(comps[2]));
        owner.setId_joc(Integer.parseInt(comps[3]));
        if (this.current_game_session.getMy_player().getPlayer_ID() == Integer.parseInt(comps[2]))
            this.current_game_session.setGame_joined(Integer.parseInt(comps[3]));
        this.current_game_session.adaugaGameOwners(owner);
        this.current_game_session.getThread_comunicatii().getLobby_frame().UpdateGamesBox();
        if (owner.id_jucator == this.current_game_session.getMy_player().getPlayer_ID()) {
            this.current_game_session.getThread_comunicatii().getLobby_frame().create_button.setText("Start");
            this.current_game_session.getThread_comunicatii().getLobby_frame().create_button_pressed = true;
            this.current_game_session.getThread_comunicatii().getLobby_frame().fereastra_joined.setPreferredSize(new Dimension(200, 200));
            this.current_game_session.getThread_comunicatii().getLobby_frame().fereastra_joined.setSize(new Dimension(200, 200));
            this.current_game_session.getThread_comunicatii().getLobby_frame().fereastra_joined.setVisible(true);
            this.current_game_session.getThread_comunicatii().getLobby_frame().fereastra_joined.setLocation(300, 500);
            this.current_game_session.getThread_comunicatii().getLobby_frame().fereastra_joined.setContentPane(this.current_game_session.getThread_comunicatii().getLobby_frame().lista_joined);
            this.current_game_session.getThread_comunicatii().getLobby_frame().fereastra_joined.pack();
            this.current_game_session.getThread_comunicatii().getLobby_frame().fereastra_joined.setAlwaysOnTop(true);
        }

    }

    //primire mesaj usrNew se adauga un nou joc in lista lobby
    public void decodeUsrNew(String message) {
        String comps[] = message.split(" ");
        Player player = new Player();
        player.setNick(comps[1]);
        player.setPlayer_ID(Integer.parseInt(comps[2]));
        this.current_game_session.adaugaLobbyPlayer(player);
        this.current_game_session.getThread_comunicatii().getLobby_frame().updateTextArea();
        this.current_game_session.getThread_comunicatii().getLobby_frame().updateTextArea();
    }

    //primire mesaj left
    public void decodeLeft(String message) {
        String comps[] = message.split(" ");
        Integer id = Integer.parseInt(comps[2]);
        this.current_game_session.getLobby_players().remove(id);
        this.current_game_session.getThread_comunicatii().getLobby_frame().updateTextArea();

    }

    //primire mesaj Joined
    public void decodeJoined(String message) {
        String comps[] = message.split(" ");
        //this.current_game_session.adaugaGamePlayer(this.current_game_session.getLobby_players().get(Integer.parseInt(comps[2])));
        this.current_game_session.getLobby_players().get(Integer.parseInt(comps[2])).setTeam(Integer.parseInt(comps[3]));
        this.current_game_session.getThread_comunicatii().getLobby_frame().UpdateListaJoined(comps[1] + " " + comps[3]);
        if (this.current_game_session.my_player.getPlayer_ID() == Integer.parseInt(comps[2]))
            this.current_game_session.getThread_comunicatii().getLobby_frame().join_button_pressed = true;
        this.current_game_session.game_joined = Integer.parseInt(comps[4]);
    }

    //primire mesaj gone
    public void decodeGone(String message) {
        String comps[] = message.split(" ");
        Integer id = Integer.parseInt(comps[2]);

        try {
            this.current_game_session.getGame_players().remove(this.current_game_session.getLobby_players().get(id).getTanc().getID());
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        if (id == this.current_game_session.getMy_player().getPlayer_ID()) {
            this.current_game_session.getGame_players().clear();
            this.current_game_session.getGame_projectiles().clear();
            this.current_game_session.getGame_walls().clear();
            this.current_game_session.addNewGameMessage("Looooozer");
            this.current_game_session.getSuprafata_desenare().paintComponent(this.current_game_session.getSuprafata_desenare().getGraphics());
            return;
        } else {
            this.current_game_session.getGame_players().remove(id);
            return;
        }

    }

    //primire messaj
    public void decodePing(String message) {
        this.current_game_session.getSuprafata_desenare().repaint();
    }

    public int decodeDirection(String message) {
        if (message.startsWith("North"))
            return 0;
        if (message.startsWith("South"))
            return 1;
        if (message.startsWith("East"))
            return 2;
        if (message.startsWith("West")) {
            return 3;
        }
        return 0;
    }


}
