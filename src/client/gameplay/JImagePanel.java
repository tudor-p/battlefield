package client.gameplay;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;

/*
 * Clasa JImagePanel 
	Aceasta clasa extinde clasa JPanel si are ca atribut o imagine bufferata care va fi flosita ca fundal pentru fereastra JintroFrame.
	 Pentru a asigura continua desenare a fotografiei de fundal am suprascris metoda paintCompoponent (Graphics g) .
 */
public class JImagePanel extends JPanel {
    protected Image game_image;

    public void setGame_image(String game_image) {
        try {
            this.game_image = ImageIO.read(getClass().getResource(game_image));
        } catch (IOException e) {
            System.err.println("Eroare incarcare imagine in JLobbyFrame:" + game_image);
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void paintComponent(Graphics g) {
        g.drawImage(game_image, 0, 0, null);
        System.out.println("S-a pictat poza de intrare");

    }

}
