/***
 * ©2009 Bogdan Balaita
 */
package client.gameplay;

import client.network.ClientThread;
import client.network.MultiClientClass;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/***
 * Clasa JIntroFrame
 Clasa extinde JFrame si este prima fereastra aparuta la lansarea aplicatiei .
 Ea contine un 2 campuri de text in care utilizatorul are posibilitatea de a scrie portul
 si adresa pe care se va conecta partea de client , si de asemenea nick-ul sub care va fi cunoscut utilizatorul pe server.
 Ea impelementeaza ActionListener pentru a asigura tratarea evenimentelor asociate butonului de start al ferestrei.
 ©2009 Bogdan Balaita
 */
public class JIntroFrame extends JFrame implements ActionListener {
    private final MultiClientClass network_client;
    protected int tried_times = 0;
    //protected JPanel content_panel;
    protected JImagePanel image_panel;
    //protected JPanel comps_panel;
    protected JTextField port;
    protected JTextField adresa;
    protected JTextField nick;
    protected JButton start_button;

    /**
     * @throws HeadlessException
     */

    public JIntroFrame() throws HeadlessException {
        super("BattleFieldGame ");
        this.network_client = new MultiClientClass();
        this.setVisible(true);
        this.image_panel = new JImagePanel();
        //this.comps_panel= new JPanel();
        this.image_panel.setGame_image("/img/intro.jpg");
        this.start_button = new JButton("Start");
        this.nick = new JTextField("nick", 12);
        this.start_button.addActionListener(this);
        this.adresa = new JTextField("192.168.0.125");
        this.port = new JTextField("39016");
        //this.content_panel= new JPanel(new FlowLayout());
        this.setContentPane(this.image_panel);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.image_panel.setPreferredSize(new Dimension(449, 311));
        //this.content_panel.add(comps_panel);
        //this.content_panel.add(image_panel);
        this.image_panel.add(adresa);

        this.image_panel.add(port);
        this.image_panel.add(nick);
        this.image_panel.add(start_button);
        this.pack();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setPreferredSize(new Dimension(330, 260));
        this.setResizable(false);


    }

    public static void main(String[] args) {
        new JIntroFrame();
    }

    public void actionPerformed(ActionEvent arg0) {

        // TODO Auto-generated method stub
        //tratarea evenimentelor asociate butoanelor
        // se va vedea cate butoane voi adauga
        ClientThread thread = null;
        this.network_client.setCurrent_nick(nick.getText());
        this.network_client.setPort(Integer.parseInt(port.getText()));
        this.network_client.setIP(adresa.getText());

        thread = this.network_client.initMultiClientClass();

        if (thread == null) {
            JLabel label = new JLabel("Eroare de conectare la server");
            label.setBounds(20, 20, 50, 200);
            label.setBackground(Color.BLACK);
            label.setSize(400, 180);
            label.setForeground(Color.RED);
            label.setPreferredSize(new Dimension(400, 180));
            label.setFont(new Font("Gigel", Font.BOLD, 28));
            this.image_panel.removeAll();
            this.image_panel.add(label);
            this.paintComponents(this.image_panel.getGraphics());


        }

        if (thread != null) {

            System.out.println("A setat threadul in controler");
            //this.controler.getLobby_frame().init();
            this.setVisible(false);
            //this.controler.getLobby_frame().setVisible(true);

            System.out.println("A reusit crearea threadului de comunicatie fereastra de intro dispare");
            thread.start();

        }

    }


}
