package client.gameplay;

import client.drawable.DrawableObject;

/*
 * Clasa Player
	Aceasta clasa incapsuleaza atributele unui utilizator aflat pe server.
	Atributele acestei clase sunt :
		protected int team=-1;
		protected String nick;
		protected int player_ID;
		protected DrawableObject tanc;
int team va retine echipa la care se aliaza un jucator;
String nick retine pseudonumele sub care este cunoscut un jucator pe server
player_ID este identificator numeric primit de la server, a carui unicitate ii ofera posibilitatea manipularii unei entitati de tip Player intr-o colectie mai mare de date.
DrawableObject tanc va retine un obiect de unul din cele 3 tipuri de tancuri si are va fi folosit pentru afisarea tancurilor pe harta.
	Metodele acestei clase sunt de tip getter si setter. 
 */
public class Player {
    //jucatorul este o entitate mai complexa
    //mare grija la manipularea lui

    //atribute
    protected int team = -1;
    protected String nick;
    protected int player_ID;
    protected DrawableObject tanc;

    public Player() {
        team = -1;
        player_ID = -1;
        tanc = null;
        nick = null;
    }

    /**
     * @return the tanc
     */
    public DrawableObject getTanc() {
        return tanc;
    }

    /**
     * @param tanc the tanc to set
     */

    public void setTanc(DrawableObject tanc) {
        this.tanc = tanc;
    }

    //methode
    public int getPlayer_ID() {
        return player_ID;
    }

    public void setPlayer_ID(int player_ID) {
        this.player_ID = player_ID;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public int getTeam() {
        return team;
    }

    public void setTeam(int team) {
        this.team = team;
    }


}
