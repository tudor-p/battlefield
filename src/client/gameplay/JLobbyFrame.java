package client.gameplay;

import client.network.ClientThread;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;

/*
 * JLobbyFrame
	Aceasta clasa , dupa cum ii spune si numele va fi folosita pentru a selecta optiunile de creare si conectare la un joc creat.
	Clasa extinde JFrame si contine un JList in care se afiseaza jucatorii existenti pe server, un JBox in care  se da posibilitatea 
	alegerii celor 2 echipe , un Jbox din care se va alege jocul la care se aliza un jucator si 3 butoane Create, Join si Cancel care dau posibilitatea utilizatorului de a selecta actiunea dorita.
In momentul in care un jucator initiaza actiunea de create va aparea o fereastra in care vorfi afisati toti userii care au dat join pentru jocul creat de user.
 */
public class JLobbyFrame extends JFrame implements ActionListener {
    /**
     *
     */
    private static final long serialVersionUID = 1697107110562646820L;
    protected ClientThread thread;
    protected JList players_area;
    protected JComboBox created_games_box;
    protected JButton join_button;
    protected JButton create_button;
    protected JComboBox team_to_choose;
    protected boolean create_button_pressed = false;
    protected boolean join_button_pressed = false;
    protected DefaultListModel listModel = new DefaultListModel();
    protected DefaultListModel list_model_joined = new DefaultListModel();
    protected JFrame fereastra_joined = new JFrame("Joined users");
    protected JList lista_joined = new JList(list_model_joined);
    protected JButton cancel_button = new JButton("Cancel");

    public JLobbyFrame(ClientThread th) {
        // TODO Auto-generated constructor stub
        super("JBattleField Lobby");
        this.thread = th;
    }

    public void init() {
        this.setTextArea();
        this.setPreferredSize(new Dimension(500, 300));
        //this.setResizable(false);
        this.setBackground(Color.BLACK);
        this.setButtons();
        this.setBoxes();
        JPanel content_panel = new JPanel(new FlowLayout());
        this.setContentPane(content_panel);
        this.cancel_button.addActionListener(this);
        content_panel.add(this.players_area);
        content_panel.add(this.create_button);
        content_panel.add(this.join_button);
        content_panel.add(this.team_to_choose);
        content_panel.add(this.created_games_box);
        content_panel.add(this.cancel_button);

        this.pack();
        this.setResizable(false);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        System.out.println("S-a apelat init la JLobbyFrame");

    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        System.out.println("S-a apasat un buton");
        if (((JButton) arg0.getSource()).equals(this.create_button) && (this.create_button_pressed == false) && (this.join_button_pressed == false)) {
            this.thread.sendMessage("create");
            this.thread.sendMessage("team " + (this.team_to_choose.getSelectedIndex() + 1));
            //this.create_button.setText("Start");
            //this.create_button_pressed=true;
            System.err.println("S-a apasat create AAAAAAAAAAAAAAA");
            this.fereastra_joined.setPreferredSize(new Dimension(200, 200));
            this.fereastra_joined.setSize(new Dimension(200, 200));
            this.fereastra_joined.setVisible(true);
            this.fereastra_joined.setLocation(300, 500);
            this.fereastra_joined.setContentPane(this.lista_joined);
            this.fereastra_joined.pack();
            this.fereastra_joined.setAlwaysOnTop(true);

            return;
        }
        if (((JButton) arg0.getSource()).equals(this.create_button) && (this.create_button_pressed == true) && (this.join_button_pressed == false)) {
            this.thread.sendMessage("start");
            this.list_model_joined.removeAllElements();
            //this.create_button_pressed=true;


            this.fereastra_joined.setVisible(false);
            return;
        }
        if ((((JButton) arg0.getSource()).equals(this.join_button)) && (this.create_button_pressed == false) && (this.join_button_pressed == false) && (this.created_games_box.getItemCount() > 0)) {
            System.out.println("Sa dam join S-a apasat join");
            String comps[] = this.created_games_box.getItemAt(this.created_games_box.getItemCount() - 1).toString().split(" ");
            int game = Integer.parseInt(comps[1]);
            this.thread.sendMessage("team " + (this.team_to_choose.getSelectedIndex() + 1));
            this.thread.sendMessage("join " + game);
            this.thread.sendMessage("start");
            //this.join_button_pressed=true;
            this.fereastra_joined.setVisible(false);
            this.list_model_joined.clear();
            this.list_model_joined.removeAllElements();
        }
        if (((JButton) arg0.getSource()).equals(this.cancel_button)) {
            this.thread.sendMessage("quit");
        }

    }

    protected void UpdateListaJoined(String s) {
        this.list_model_joined.addElement(s);
        this.fereastra_joined.repaint();
    }

    public void setTextArea() {


        for (Map.Entry<Integer, Player> entry : this.thread.getMyGameSession().getLobby_players().entrySet()) {
            listModel.addElement(entry.getValue().getNick());
        }
        players_area = new JList(listModel);
        this.players_area.setSize(new Dimension(120, 250));
        this.players_area.setPreferredSize(new Dimension(120, 250));

    }

    public void updateTextArea() {
        listModel.removeAllElements();
        if (this.players_area != null) {
            this.players_area.removeAll();
            for (Map.Entry<Integer, Player> entry : this.thread
                    .getMyGameSession().getLobby_players().entrySet()) {
                this.listModel.addElement(entry.getValue().getNick());
            }
        }
        this.players_area.setModel(listModel);
        this.players_area.repaint();
    }

    public void setButtons() {
        this.create_button = new JButton("Create");
        this.join_button = new JButton("Join");
        this.create_button.addActionListener(this);
        this.join_button.addActionListener(this);

    }

    public void setBoxes() {
        String[] teams = {"1", "2"};
        this.created_games_box = new JComboBox();
        this.created_games_box.setPreferredSize(new Dimension(90, 25));
        this.team_to_choose = new JComboBox(teams);
        for (Map.Entry<Integer, GameOwners> entry : this.thread.getMyGameSession().getGame_owners().entrySet()) {
            this.created_games_box.addItem(entry.getValue().getNick() + " " + entry.getValue().getId_joc());
        }
    }

    public void UpdateGamesBox() {
        this.created_games_box.removeAllItems();
        for (Map.Entry<Integer, GameOwners> entry : this.thread.getMyGameSession().getGame_owners().entrySet()) {
            this.created_games_box.addItem(entry.getValue().getNick() + " " + entry.getValue().getId_joc());
        }
        this.created_games_box.repaint();
        this.created_games_box.repaint();
    }


}
