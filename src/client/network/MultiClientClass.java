package client.network;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

/*
 * Clasa MultiClientClass
Aceasta clasa  este responsabila pentru stabilirea conexiunii cu serverul prin 
intermediu metodei initMultiClientClass care deschide un socket la adresa si portul spoecificat.Incercarea de a lua legatura cu serverul se face de 3 ori dupa care daca se esueaza se afiseaza un mesaj care sa specifice esecul.
Atributele acestei clase sunt:
port
socket_legat_la_server
IP
current_nick
current_team

Metodele clasei sunt initMultiClientClass, si cele de tip geter si setter
 */
public class MultiClientClass {
    protected Socket socket_legat_la_server = null;
    protected String IP;
    protected String current_nick;
    protected int current_team;
    int port;

    public MultiClientClass(int port, String IP, int team, String nick) {
        this.port = port;
        this.IP = IP;
        current_team = team;
        current_nick = nick;
    }

    public MultiClientClass() {
        // TODO Auto-generated constructor stub
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getIP() {
        return IP;
    }

    public void setIP(String ip) {
        IP = ip;
    }

    public String getCurrent_nick() {
        return current_nick;
    }

    public void setCurrent_nick(String current_nick) {
        this.current_nick = current_nick;
    }

    public ClientThread initMultiClientClass() {
        int i = 0;
        ClientThread thread = null;
        while (true) {
            try {
                this.socket_legat_la_server = new Socket(IP, port);

                System.out.println("S-a conectat la server");

                thread = new ClientThread(socket_legat_la_server, current_nick); //initializare thread de comunicare cu serverul
                if (thread != null) {
                    System.out.println("Se returneaza threadul");
                    return thread;
                }
                break;
            } catch (UnknownHostException e) {
                System.err.println("Nu s-a putut conecta la server");
                // break;
            } catch (IOException e) {

                System.err.println("Nu a putut obtine comunicare I/O cu serverul");
                ++i;
                if (i == 3) {
                    break;
                }

            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
        System.out.println("S-a obtinut threadul" + thread);
        return thread;
    }

}
