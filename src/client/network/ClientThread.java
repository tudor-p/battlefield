package client.network;

import client.gameplay.GameSession;
import client.gameplay.JBattleFieldFrame;
import client.gameplay.JLobbyFrame;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.net.Socket;

/*
 * Clasa ClientThread este cea care se  ocupa de comunicarea cu serverul , iar mesajele primite de la acesta sunt transmise sesiunii de joc care prin intermediul unui interpretor  face updatarile vehiculate pe retea.
Atributele acestei clase sunt:
	-GameSession my_game;
	-protected Socket socket;	
	-private BufferedReader in;
	-private PrintWriter out;
	-private boolean client_playing=true;
	-private boolean client_in_lobby=true;
	-protected JBattleFieldFrame battle_field_frame;
	-protected JLobbyFrame lobby_frame;

GameSession my_game reprezinta o referinta la clasa care se va ocupa de managementul propriu-zis al jocului.
Socket socket este canalul de comunicatie cu serverul.
BufferedReader in si PrintWriter out reprezinta cele 2 streamuri bufferate prin care se realizeaza comunicatia cu serverul, ele fiind cele atasate socketului;
JbattleFiledFrame este o fereastra pe care se va jocul propriu-zis
JlobbyFrame este o fereastra prin care se realizeaza optiunile utilizator.
 Metodele acestei clase sunt:
-sendMessage(String message) este metoda care se ocupa de trimiterea mesajului dat ca parametru catre server
-receiveMessage() este metoda care ocupa de receptionarea mesajelor de la server
-initBuffers() prin care se realizeaza initilizarea celor 2 streamuri.
Intrucat clasa extinde clasa Thread aceasta implementeaza metoda run().In cadrul acestei metode exita 2 bucle care au rolul de a asigura comunicarea cu serverul pana la aparitia unei exceptii care sa inchida socketul de comunicatie.
La toatre acestea se adauga metodele getter si setter care au fost implementate automat folosind facilitatile Eclipse .
 */
public class ClientThread extends Thread {


    protected Socket socket;
    protected JBattleFieldFrame battle_field_frame;
    protected JLobbyFrame lobby_frame;
    GameSession my_game;
    private BufferedReader in;
    private PrintWriter out;
    private boolean client_playing = true;
    private boolean client_in_lobby = true;


    //constructor
    public ClientThread(Socket socket, String nick) {
        //  Auto-generated constructor stub
        //constructorul clasei de comunicatie
        this.socket = socket;
        this.my_game = new GameSession(this);
        this.my_game.getMy_player().setNick(nick);
        initBuffers();
        lobby_frame = new JLobbyFrame(this);
        System.out.print("S-a creat si lobyul");
        battle_field_frame = new JBattleFieldFrame(this, this.my_game.getSuprafata_desenare());
        this.lobby_frame.init();
        this.lobby_frame.setVisible(true);
        this.lobby_frame.paint(this.lobby_frame.getGraphics());

        //this.run();
    }


    public void initBuffers() {
        try {
            out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        try {
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    //metoda prin care se trimite catre server informatii
    public void sendMessage(String message) {
        out.println(message);
        System.out.println("In message s-a trimis   " + message);
    }

    //metoda de obtinere informatii de la server
    public String receiveMessage() {
        String message = null;
        try {
            message = in.readLine();
        } catch (IOException e) {
            System.err.println("Nu s-a putut receptiona date de la server");
            this.lobby_frame.setVisible(false);
            this.battle_field_frame.setVisible(false);
            JFrame errFrame = new JFrame("Eroare");
            JLabel label = new JLabel("Eroare comunicare cu serverul");
            label.setForeground(Color.RED);
            label.setSize(250, 160);
            errFrame.add(label);
            errFrame.pack();
            errFrame.setPreferredSize(new Dimension(320, 240));
            errFrame.setSize(320, 240);
            e.printStackTrace();
            errFrame.setVisible(true);
            errFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            errFrame.setAlwaysOnTop(true);
            this.interrupt();
        }
        if (message == null) {
            this.lobby_frame.setVisible(false);
            this.battle_field_frame.setVisible(false);
            JFrame errFrame = new JFrame("Eroare");
            JLabel label = new JLabel("Eroare ");
            errFrame.add(label);
            errFrame.pack();
            errFrame.setPreferredSize(new Dimension(320, 240));
        }
        return message;
    }

    public void run() {
        String message;

        sendMessage("nick " + this.my_game.getMy_player().getNick() + " 1");

        System.out.println("A intrat in run de la thread");

        //sendMessage("nick "+this.getMyGameSession().getMy_player().getNick());
        while (this.client_in_lobby) {
            //jucatorul a intrat in jocul BattleField
            // nu neaparat joaca  ci poate asteapta sa intre in joc
            //bucla este utilizata pentru a putea relua jocul dupa ce am terminat de jucat cu tanculetele
            while (this.client_playing) {
                System.out.println("S-a revenit in joc");

                //bucla de jucare
                //se termian de fiecare data knd se termina un joc de tancuri
                message = receiveMessage();
                this.my_game.getDecoder().decodeMessage(message);
                this.lobby_frame.paint(this.lobby_frame.getGraphics());
                if (message.startsWith("gameOver"))
                    this.client_playing = false;
            }
            this.client_playing = true;
            //this.my_game.getGame_projectiles().clear();
            //this.my_game.getGame_players().clear();
            //this.my_game.getGame_walls().clear();
        }
        try {
            this.socket.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public boolean isClientInLobby() {
        return client_in_lobby;
    }

    public void setClient_in_lobby(boolean client_in_lobby) {
        this.client_in_lobby = client_in_lobby;
    }

    public boolean isClientPlaying() {
        return client_playing;
    }

    public void setClient_playing(boolean client_playing) {
        this.client_playing = client_playing;
    }

    public GameSession getMyGameSession() {
        return my_game;
    }

    public JBattleFieldFrame getBattle_field_frame() {
        return battle_field_frame;
    }

    public JLobbyFrame getLobby_frame() {
        return lobby_frame;
    }


}
