import client.gameplay.JIntroFrame;
import server.game.GamesAndPlayersManager;

import javax.swing.*;
import java.awt.event.ActionEvent;

/***
 * MainWindow Entry point for starting client/server_thread sub applications
 * It integrates the two separated client and server_thread apps
 * ©2019 Tudor Popovici
 */
public class MainWindow extends JIntroFrame {
    private final JButton btn_start_server;
    private Thread server_thread = null;

    public MainWindow() {
        super();
        this.start_button.setText("Start Client");
        this.btn_start_server = new JButton("Start Server");
        this.btn_start_server.addActionListener(this);
        this.adresa.setText("127.0.0.1");
        this.image_panel.add(btn_start_server);
        this.pack();
    }

    public static void main(String[] args) {
        new MainWindow();
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        if (arg0.getSource() == this.btn_start_server) {
            if (null == this.server_thread) {
                //Handle starting the server_thread request
                this.server_thread = new Thread(new GamesAndPlayersManager(this.port.getText()));
                this.server_thread.start();
                this.btn_start_server.setText("Stop Server");
            } else {
                this.server_thread.interrupt();
                this.btn_start_server.setText("Start Server");
                this.server_thread = null;
            }
        } else {
            //forward the event
            super.actionPerformed(arg0);
        }
    }

}
