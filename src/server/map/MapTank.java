package server.map;

import server.game.Player;

// clasa tank
// instantiata de Spatiul de joc (MapSpaceEngine)
// si atribuita fiecarui jucator participant
public class MapTank extends MapObjectMovable {
    // dimensiunea laturii tancului
    public static final int DIMENSION = 3;
    // dimensiunea specifica
    protected static final MapCoordinate dim = new MapCoordinate(DIMENSION, DIMENSION);
    // divizor de viteza
    private static final int latency = 3;
    // starea de foc a tunului
    protected boolean firing;
    // este tunul incarcat?
    protected boolean ready_to_fire = true;
    // contor pt divizorul de viteza
    private int latencycounter = 0;

    // ctor, apelat de SpaceEngineul responsabil
    public MapTank(Player owner, MapSpaceEngine mapSpace) {
        this.space = mapSpace;
        this.owner = owner;
        this.pos = new MapCoordinate(-1, -1);
        // a nu se folosi setterul setDir aici, declanseaza redesenare
        // va fi gasit pos pe null
        if (owner.getTeam() % 2 == 0) {
            dir = MapDirection.North;
        } else {
            dir = MapDirection.South;
        }
        owner.setTank(this);
        moving = false;
    }

    // getter pentru dimensiune
    @Override
    public MapCoordinate getDim() {
        return dim;
    }

    // se verifica daca tunul poate trage, adica daca nu se afla cu tunul in afara hartii
    public boolean canFire() {
        if (isNear()) {
            return (space.getMapObject(getPos().translate(getDir())) != MapSpaceEngine.LIMIT);
        } else {
            return (space.getMapObject(getPos().translate(getDim().x - 1, getDim().y - 1).translate(getDir())) != MapSpaceEngine.LIMIT);
        }
    }

    // tragaciul tunului, returneaza true daca a tras la cererea curenta
    public boolean Fire() {
        if (firing && ready_to_fire && canFire()) {
            new MapProjectile(this);
            return true;
        }
        return false;
    }

    // reincarca tunul, de catre proiectilul anterior care a disparut
    public synchronized void setReady(boolean rdy) {
        ready_to_fire = rdy;
    }

    // reactia proiectilului la incercarea de miscare
    @Override
    public boolean Move() {
        boolean f = Fire();
        if (moving && (++latencycounter % latency == 0)) {
            if (!isOnCollissionStep()) {
                space.unmarkObject(this);
                setPos(getPos().translate(getDir()));
                space.markObject(this);
                space.redraw(this);
                return true;
            }
        }
        return f;
    }

    // setter pentru directie
    @Override
    public void setDir(MapDirection d) {
        // filtrarea schimbarii de directie
        if (d != dir) {
            dir = d;
            space.redraw(this);
            owner.getGame_session().Broadcast("ping");
        }
    }

    // metoda de serializare
    @Override
    public String toString() {
        return super.toString() + " " + owner.id + " " + owner.getTeam();
    }

    // reactie la coliziunea cu proiectil
    // utilizata si la stergerea tancurilor in cazul pierderei bazei proprii
    @Override
    public void Collision(MapProjectile boomer) {
        System.out.println("Tank distroyed: " + owner);
        super.erase();
        owner.GameOver();
    }

    // raspuns la cererea de foc, apelat de interpretorul de comenzi
    public void startFire() {
        firing = true;
        //space.redraw(this);//unnecesary
    }

    // raspuns la incetarea focului, apelat de interpretorul de comenzi
    public void stopFire() {
        firing = false;
        //space.redraw(this);//unnecesary
    }

    // metoda de localizare a punctului de plecare a proiectilului, functie de orientarea proiectilului
    public MapCoordinate getArround(MapDirection d) {
        switch (MapDirection.index(d)) {
            case 0:
                return new MapCoordinate(dim.x / 2, -1);
            case 1:
                return new MapCoordinate(dim.x / 2, dim.y);
            case 2:
                return new MapCoordinate(dim.x, dim.y / 2);
            case 3:
                return new MapCoordinate(-1, dim.y / 2);
        }
        return null;
    }

    // reactie la incercarea de a misca tancul, apelat de interpretorul de comenzi
    public void setMoving(boolean b) {
        // TODO Auto-generated method stub
        moving = b;
    }

    // interogheaza stara tunului
    public boolean isReady_to_fire() {
        return ready_to_fire;
    }
}
