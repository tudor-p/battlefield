package server.map;

// clasa responsabila cu "mantisarea" hartii
// este intors la cerere de obiecte aflate in afara suprafetei hartii
public class MapLimit extends MapObject {
    // dimensiune specifica, nula
    protected static final MapCoordinate dim = new MapCoordinate(0, 0);//adimensionala :D

    // ctor, almost useless...
    public MapLimit(MapCoordinate c, MapSpaceEngine n) {
        setPos(c);
        space = n;
    }

    // reactie la impact, nimic, doar neutralizeaza proiectilul
    @Override
    public void Collision(MapProjectile boomer) {
    }

    //pentru compatibilitate... intoarce dimensiunea
    @Override
    public MapCoordinate getDim() {
        return dim;
    }
}
