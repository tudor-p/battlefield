package server.map;

import server.game.GameSession;
import server.game.Player;

import java.util.ArrayList;

// clasa MapSpaceEngine
// administreaza obiectele de pe harta
// genereaza tancuri pt toti jucatorii
// trimite harta jucatorilor
// interpreteaza proprietatile obiectelor, le pune in miscare
//
public class MapSpaceEngine extends Thread {
    // dimensiunile in unitati elementare de harta
    public final static int MAXX = 60;
    public final static int MAXY = 70;
    // instanta de "mantisare" a hartii
    public final static MapObject LIMIT = new MapLimit(new MapCoordinate(-1, -1), null);
    // matricea de evidenta a obiectelor pe harta
    private MapObject[][] map = new MapObject[MAXX][MAXY];
    // lista de obiecte mobile aflate pe harta
    private ArrayList<MapObjectMovable> movable = new ArrayList<MapObjectMovable>();
    // sesiunea de joc parinte pentru spatiul de joc
    private GameSession gameSession;
    // starea de joc
    private boolean started;

    // ctor, apelat din game session
    public MapSpaceEngine(GameSession g) {
        gameSession = g;
        for (int x = 0; x < MAXX; ++x) {
            for (int y = 0; y < MAXY; ++y) {
                map[x][y] = null;
            }
        }
    }

    // getter pt game sessionul parinte
    public GameSession getGameSession() {
        return gameSession;
    }

    // metoda de desenare unitara a zidurilor mari
    private void putWall(int x, int y0, int w, int h) {
        int lx = x + w;
        int ly = y0 + h;
        int y = y0;
        for (; x < lx; ++x, y = y0) {
            for (; y < ly; ++y) {
                new MapWall(this, x, y);
            }
        }
    }

    // metoda de pornire a jocului
    // genereaza harta
    // expediaza pozitiile tancurilor
    public void Start() {
        started = true;

        paintMap();

        for (MapObjectMovable m : movable) {
            redraw(m);
        }
        gameSession.Broadcast("ping");
        start();
    }

    // opreste jocul curent la cerere
    public void Stop() {
        started = false;
    }

    // firul de management a logicii jocului
    // responsabil cu miscarea obiectelor, si interpretarea starii acestora
    @Override
    public void run() {
        ArrayList<MapObjectMovable> lst = new ArrayList<MapObjectMovable>();
        while (started) {
            boolean ping = false;
            synchronized (movable) {
                lst.clear();
                lst.addAll(movable);
            }
            for (MapObjectMovable m : lst) {
                ping = ping || m.Move();
            }
            if (ping) {
                gameSession.Broadcast("ping");
            }
            try {
                Thread.sleep(30);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                System.out.print("Wait exc " + e.toString());
                e.printStackTrace();
            }
        }
    }

    // MapSpaceEngine are rol de fabrica de tancuri pt utilizatori, apelat din game session
    public void getTank(Player l) {
        // chiar e nevoie???????????
        if (l.getTank() != null) {
            remMovable(l.getTank());
        }
        MapTank t = new MapTank(l, this);
        if (l.getTeam() % 2 != 0) {
            t.setPos(new MapCoordinate(MAXX - MapTank.DIMENSION, 0));
            while (t.getPos().x >= 0 && t.willCollide()) {
                --t.getPos().x;
            }
        } else {
            t.setPos(new MapCoordinate(0, MAXY - MapTank.DIMENSION));
            while (t.getPos().x <= MAXX - MapTank.DIMENSION && t.willCollide()) {
                ++t.getPos().x;
            }
        }
        if (!t.willCollide()) {
            addMovable(t);
        }
    }

    // metoda de identificare a obiectelor aflate la coordonate date
    public MapObject getMapObject(int x, int y) {
        if (x < 0 || y < 0 || x >= MAXX || y >= MAXY) {
            return LIMIT;
        }
        return map[x][y];
    }

    // supraincarcarea metodei de localizare a obiectului de la coordonatele date
    public MapObject getMapObject(MapCoordinate c) {
        return getMapObject(c.x, c.y);
    }

    // metoda de marcare a existentei obiectului in matricea de evidenta a obiectelor pe harta
    public synchronized void markObject(MapObject obj) {
        final MapCoordinate d = obj.getDim();
        for (int i = 0; i < d.x; ++i) {
            for (int j = 0; j < d.y; ++j) {
                map[obj.getPos().x + i][obj.getPos().y + j] = obj;
            }
        }
        obj.setMarked(true);
    }

    // metoda de demarcare a obiectelor din matricea de evidenta a hartii
    public synchronized void unmarkObject(MapObject obj) {
        if (obj.isMarked()) {
            for (int i = 0; i < obj.getDim().x; ++i) {
                for (int j = 0; j < obj.getDim().y; ++j) {
                    map[obj.getPos().x + i][obj.getPos().y + j] = null;
                }
            }
            obj.setMarked(false);
        }
    }

    // metoda de inregistrare a unui nou obiect mobil pe harta
    public synchronized void addMovable(MapObjectMovable mobil) {
        synchronized (movable) {
            movable.add(mobil);
        }
        markObject(mobil);
    }

    // metoda de eliminare a unui obiect mobil din evidenta hartii
    public synchronized void remMovable(MapObjectMovable mobil) {
        synchronized (movable) {
            movable.remove(mobil);
        }
    }

    // trimite comanda de redesenare a obiectului catre clientii jocului
    public void redraw(MapObject ob) {
        gameSession.Broadcast("paint " + ob.toString());
    }

    // trimite comanda de eliminare a obictului din evidenta clientilor
    public void erase(MapObject ob) {
        gameSession.Broadcast("erase " + ob.toString());
    }

    // cand o baza este distrusa toti jucatorii din echipa respectiva primesc gone
    // si le sunt distruse tancurile
    public void BaseLost(int loosingTeam) {
        ArrayList<MapTank> players = new ArrayList<MapTank>();
        for (MapObjectMovable l : movable) {
            if (l.getClass().getName().equals(MapTank.class.getName())) {
                players.add((MapTank) l);
            }
        }
        for (MapTank l : players) {
            if (l.getOwner().getTeam() == loosingTeam) {
                //collides with he's own stupidity :D
                l.getOwner().getTank().Collision(null);
                //l.owner.GameOver();
            }
        }
    }

    // metoda responsabila de plasarea obiectelor statice pe harta
    private void paintMap() {
        int bcx = MAXX / 2;
        int bcy = 1;
        new MapBase(1, this, bcx, bcy);//echipa 1 sus
        int ww = 2;
        int ws = 1;
        putWall(bcx - ww - ws, 0, ww, MapBase.dim.y + bcy + ws + ww);
        putWall(bcx + MapBase.dim.x + ws, 0, ww, MapBase.dim.y + bcy + ws + ww);
        putWall(bcx - ws, bcy + MapBase.dim.y + ws, ws + MapBase.dim.x + ws, ww);
        bcy = MAXY - MapBase.dim.y - 1;
        new MapBase(2, this, bcx, bcy);//echipa 2 jos
        putWall(bcx - ww - ws, bcy - ww - ws, ww, MapBase.dim.y + ww + ws + ws + 1);
        putWall(bcx + MapBase.dim.x + ws, bcy - ww - ws, ww, MapBase.dim.y + ww + ws + ws + 1);
        putWall(bcx - ws, bcy - ww - ws, ws + MapBase.dim.x + ws, ww);

        putWall(0, MAXY / 2, MAXX, 4);
    }
}
