package server.map;

// clasa responsabila de managementul actiunii si existentei unui proiectil
// in relatie cu tancul posesor
public class MapProjectile extends MapObjectMovable {
    // raza de actiune //radius  21012
    public final static int DAMAGE_RADIUS = 1;
    // dimensiuni specifice
    protected static final MapCoordinate dim = new MapCoordinate(1, 1);
    // tancul emitent
    private MapTank tankowner;

    // ctor
    // incearca crearea unui nou proiectil, daca este chiar lanaga un perete sau alt obiect
    // proiectilul nu mai este inregistrat
    // este luat in calcul doar efectul lui
    public MapProjectile(MapTank owner) {
        owner.setReady(false);
        space = owner.space;
        tankowner = owner;
        //setDim(new MapCoordinate(1,1));
        setDir(owner.getDir());
        setPos(owner.getPos().translate(owner.getArround(getDir())));
        moving = true;
        if (!TryCollide(true)) {
            space.addMovable(this);
            space.redraw(this);
        } else {
            owner.setReady(true);
        }
    }

    // getter pentru dimensiune
    @Override
    public MapCoordinate getDim() {
        return dim;
    }

    // se incearca coliziunea la pasul curent
    // unmarked specifica daca proiectilul a fost desenat, si se cauta obstacole in fata
    // sau daca este in curs de creare, si se cauta obstacol pe pozitia curenta
    // returneaza true pentru coliziune pe traiectoria curenta
    private boolean TryCollide(boolean unmarked) {
        MapObject ob = null;
        MapCoordinate xp = null;
        if (unmarked) {
            xp = getPos();//the projectile is not market yet on the map
        } else {
            xp = getPos().translate(getDir());//the projectile is marked and moving
        }
        if ((ob = space.getMapObject(xp)) != null) {
            ob.Collision(this);

            if (MapDirection.isVertical(getDir())) {
                for (int i = 1; i <= DAMAGE_RADIUS; ++i) {
                    if ((ob = space.getMapObject(xp.translate(i, 0))) != null) {
                        //to avoid crushing tanks when the nearby objects gets hit
                        if (!(ob.getClass().getName().equals(MapTank.class.getName()))) {
                            ob.Collision(this);
                        }
                    }
                    if ((ob = space.getMapObject(xp.translate(-i, 0))) != null) {
                        if (!(ob.getClass().getName().equals(MapTank.class.getName()))) {
                            ob.Collision(this);
                        }
                    }
                }
            } else {
                for (int i = 1; i <= DAMAGE_RADIUS; ++i) {
                    if ((ob = space.getMapObject(xp.translate(0, i))) != null) {
                        if (!(ob.getClass().getName().equals(MapTank.class.getName()))) {
                            ob.Collision(this);
                        }
                    }
                    if ((ob = space.getMapObject(xp.translate(0, -i))) != null) {
                        if (!(ob.getClass().getName().equals(MapTank.class.getName()))) {
                            ob.Collision(this);
                        }
                    }
                }
            }
            return true;
        }
        return false;
    }

    // getter pentru tancul emitent
    public MapTank getTank() {
        return tankowner;
    }

    // metoda de stergere din spatiul de joc
    @Override
    protected void erase() {
        super.erase();
        this.tankowner.setReady(true);
    }

    // reactie la incercarea de a avansa proiectilul
    @Override
    public boolean Move() {
        //incerc sa distrug tot ce prind, vezi tryCollide
        if (TryCollide(false)) {
            this.erase();
        } else {
            space.unmarkObject(this);
            setPos(getPos().translate(getDir()));
            space.markObject(this);
            space.redraw(this);
        }
        return true;
    }

    //reactia la coliziunea cu alte obiecte distructive
    @Override
    public void Collision(MapProjectile boomer) {
        //pt ca nu toate obiectele crapa la impact!
        this.erase();
    }
}
