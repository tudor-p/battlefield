package server.map;

// clasa care desemneaza proprietatile specifice obiectelor mobile pe harta
// permite deplasarea lor
public abstract class MapObjectMovable extends MapObject {
    // starea de miscare a obiectului
    protected boolean moving;

    // reactia obiectului la incercarea de miscare
    public abstract boolean Move();

    // completez procedura de stergere a obiectului generic
    @Override
    protected void erase() {
        super.erase();
        space.remMovable(this);
    }

    // verifica daca tre adaugat deplasament suplimentar pt calculul coordonatelor
    protected boolean isNear() {
        if (getDir() != MapDirection.North && getDir() != MapDirection.West) {
            return false;
        }
        return true;
    }

    // verifica daca pe directia curenta exista obstacole
    public boolean isOnCollissionStep() {
        //  NNNN  x=origin
        // WxoooE
        // WooooE
        // WooooE
        //  SSSS
        MapCoordinate o = new MapCoordinate(getPos()), lim = null;
        MapCoordinate offset = null;
        if (getDir().equals(MapDirection.North)) {
            --o.y;
            lim = o.translate(getDim().x, 0);
        }
        if (getDir().equals(MapDirection.South)) {
            o.y += getDim().y;
            lim = o.translate(getDim().x, 0);
        }
        if (getDir().equals(MapDirection.West)) {
            --o.x;
            lim = o.translate(0, getDim().y);
        }
        if (getDir().equals(MapDirection.East)) {
            o.x += getDim().x;
            lim = o.translate(0, getDim().y);
        }
        if (MapDirection.isVertical(getDir())) {
            offset = new MapCoordinate(1, 0);
        } else {
            offset = new MapCoordinate(0, 1);
        }
        while (!o.equals(lim)) {
            if (space.getMapObject(o) != null) {
                return true;
            }
            o.x += offset.x;
            o.y += offset.y;
        }
        return false;
    }
}
