package server.map;

// baza
//
// proprietati speciale
// poate fi distrusa
// conduce la invingerea echipei asociate
public class MapBase extends MapObject {
    // dimensiune specifica
    public static final MapCoordinate dim = new MapCoordinate(3, 3);
    // echipa asociata 1 sau 2
    private int team = 2;

    // ctor primeste echipa, spatiul si cooordonatele
    public MapBase(int team, MapSpaceEngine ms, int x, int y) {
        this.team = team;
        this.setOwner(null);
        this.space = ms;
        if (y < MapSpaceEngine.MAXY / 2) {
            setDir(MapDirection.South);
        } else {
            setDir(MapDirection.North);
        }
        setPos(new MapCoordinate(x, y));
        if (!willCollide()) {
            space.markObject(this);
            space.redraw(this);
        }
    }

    // administrarea evenimentului de coliziune cu un proiectil
    @Override
    public void Collision(MapProjectile boomer) {
        erase();
        space.BaseLost(team);
    }

    // getter pentru dimensiune
    @Override
    public MapCoordinate getDim() {
        return dim;
    }
}
