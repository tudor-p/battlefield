package server.map;

// clasa pentru incapsularea coordonatelor si dimensiunilor
// suporta translare si diferiti constructori
// translare dupa directie
public class MapCoordinate extends Object {
    // accesibilitate publica pentru coordonate
    // usureaza manevrele
    public int x, y;

    // ctor implicit
    public MapCoordinate() {
        x = y = 0;
    }

    // ctor cu initializare
    public MapCoordinate(int x, int y) {
        this.x = x;
        this.y = y;
    }

    // ctor de copiere
    public MapCoordinate(MapCoordinate pos) {
        this.x = pos.x;
        this.y = pos.y;
    }

    // comparator
    public boolean equals(MapCoordinate o) {
        return (x == o.x && y == o.y);
    }

    // copiere cu translare cu alta coordonata
    public MapCoordinate translate(MapCoordinate d) {
        MapCoordinate n = new MapCoordinate(x, y);
        n.x += d.x;
        n.y += d.y;
        return n;
    }

    // copiere cu translare cu coordonate explicite
    public MapCoordinate translate(int xx, int yy) {
        MapCoordinate n = new MapCoordinate(x, y);
        n.x += xx;
        n.y += yy;
        return n;
    }

    // copiere cu translare cu vector de directie
    public MapCoordinate translate(MapDirection d) {
        MapCoordinate n = new MapCoordinate(x, y);
        switch (MapDirection.index(d)) {
            case 0:
                --n.y;
                break;
            case 1:
                ++n.y;
                break;
            case 2:
                ++n.x;
                break;
            case 3:
                --n.x;
                break;
        }
        return n;
    }

    // metoda de serializare
    @Override
    public String toString() {
        return x + " " + y;
    }
}
