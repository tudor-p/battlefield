package server.map;

// clasa Wall
// obstacol distructibil
public class MapWall extends MapObject {
    // dimensiunea specifica
    protected static final MapCoordinate dim = new MapCoordinate(1, 1);

    // ctor
    public MapWall(MapSpaceEngine se, int x, int y) {
        setPos(new MapCoordinate(x, y));
        setDir(MapDirection.North);
        space = se;
        if (!willCollide()) {
            se.markObject(this);
            se.redraw(this);
        }
    }

    // reactie la coliziunea unui proiectil
    @Override
    public void Collision(MapProjectile p) {
        this.erase();
    }

    // getter pt dimensiune
    @Override
    public MapCoordinate getDim() {
        return dim;
    }
}
