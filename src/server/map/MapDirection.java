package server.map;

// enum responsabil de orientarea obiectelor pe harta
public enum MapDirection {
    // directii
    North, South, East, West;

    // interogare directie verticala
    public static boolean isVertical(MapDirection dir) {
        if (dir != North && dir != South) {
            return false;
        }
        return true;
    }

    // identifica indexul asociat directiei
    public static int index(MapDirection d) {
        MapDirection[] dirs = MapDirection.values();
        for (int i = 0; i < dirs.length; ++i) {
            if (d == dirs[i]) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public String toString() {
        return name();
    }
}
