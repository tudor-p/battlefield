package server.map;

import server.game.Player;

// clasa care defineste proprietatile comune ale tuturor obiectelor de pe harta
public abstract class MapObject extends Object {
    // contorul de instante, generator de id unic
    private static int id_generator = 0;
    // id unic asociat
    final protected int id = ++id_generator;
    // proprietarul obiectului, daca acesta are proprietar
    protected Player owner;
    // spatiul si logica jocului responsabila de dinamica obiectului
    protected MapSpaceEngine space;
    // pozitia obiectului in spatiul de joc
    protected MapCoordinate pos;
    // orientarea obiectului pe harta
    protected MapDirection dir;
    // a fost sau nu marcat obiectul in matricea din spatiul de joc
    protected boolean marked = false;

    // metoda de serializare
    @Override
    public String toString() {
        return new String(getClass().getSimpleName() + " " + id + " " + pos + " " + dir);
    }

    // metoda care verifica posibile coliziuni, inaintea marcarii obiectului in matricea spatiului
    public boolean willCollide() {
        MapCoordinate dim = getDim();
        for (int x = 0; x < dim.x; ++x) {
            for (int y = 0; y < dim.y; ++y) {
                if (space.getMapObject(pos.translate(x, y)) != null) {
                    return true;
                }
            }
        }
        return false;
    }

    // metoda de stergere a obiectului
    protected void erase() {
        space.unmarkObject(this);
        space.erase(this);
    }

    // reactia obiectului la coliziunea cu un proiectil
    public abstract void Collision(MapProjectile boomer);

    // getterul pentru dimensiune
    public abstract MapCoordinate getDim();

    public Player getOwner() {
        return owner;
    }

    // getteri/setteri
    public void setOwner(Player owner) {
        this.owner = owner;
    }

    public MapDirection getDir() {
        return dir;
    }

    public void setDir(MapDirection dir) {
        this.dir = dir;
    }

    public boolean isMarked() {
        return marked;
    }

    public void setMarked(boolean marked) {
        this.marked = marked;
    }

    public MapCoordinate getPos() {
        return pos;
    }

    public void setPos(MapCoordinate pos) {
        this.pos = pos;
    }
}
