package server.network;

// interfata pentru manipularea unui numar de clienti cu proprietati comune de tip ClientThreadA
public interface IClientManagement extends IBroadCastDomain {
    // metoda apelata de client la stabilirea conexiunii
    void ClientConnected(AClientThread client);

    // metoda apelata de client la terminarea conexiunii
    void ClientDisconnected(AClientThread client);
}
