package server.network;

import java.io.*;
import java.net.Socket;

// clasa abstracta pentru interfatatarea cu lucrul pe retea, administreaza sesiuni de comunicatii
//
// ofera un client generic cu posibilitati de a primi si transmite mesaje
// clientii pot fi administrati uniform print-un manager de clienti care implementeaza IClientManager
// clientii pot fi generati de o fabrica de obiecte care implementeaza IClientThreadFactory
public abstract class AClientThread extends Thread {
    //private ObjectOutputStream outs;
    //private ObjectInputStream ins;
    protected boolean running;
    private Socket socket;
    private BufferedReader in;
    private PrintWriter out;

    // este instantiata printr-o fabrica de obiecte, de regula la cererea unui server
    //		care a acceptat o noua conexiune
    public AClientThread(Socket s) {
        socket = s;
        running = true;
        try {
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            //ins = new ObjectInputStream(socket.getInputStream());
            // Enable auto-flush:
            out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
            //outs = new ObjectOutputStream(socket.getOutputStream());
        } catch (Exception e) {
            System.out.println("Buffer initializasions failled " + e.toString());
            e.printStackTrace();
            return;
        }
        // If any of the above calls throw an
        // exception, the caller is responsible for
        // closing the socket. Otherwise the thread
        // will close it.
    }

    // porneste propriu-zis modulul de pasare a mesajelor
    // permite amanarea pasarii mesajelor, pentru a permite sincronizarea cu alte evenimente
    //	din aplicatie
    public void Start() {
        start(); // Calls run()
        OnClientConnected();
    }

    // evenimentul de conectare
    public abstract void OnClientConnected();

    // evenimentul de primire de date noi
    public abstract void Recv(String str);

    // metoda de trimitere de date noi
    public final void Send(String str) {
        //out.println(str);
        try {
            if (str != null) {
                System.out.println("Sent: " + str);
                //outs.writeObject(str);
                out.println(str);
            } else {
                System.out.println("Null Sending!");
            }
        } catch (Exception e) {
            System.out.println("Object sending error");
            e.printStackTrace();
        }
    }

    // metoda de inchidere a sesiunii de comunicatii
    public void Close() {
        running = false;
    }

    // fir de executie responsabil cu primirea noilor mesaje
    public void run() {
        try {
            String str;
            do {
                str = in.readLine();
                //str = (String)ins.readObject();
                if (str != null) {
                    System.out.println("Recvd: " + str);
                    try {
                        Recv(str);
                    } catch (Exception e) {
                        System.out.println(e.toString());
                        e.printStackTrace();
                    }
                }
            }
            while (running);
            System.out.println("closing...");
            //cmng.ClientDisconnected(this);
            //TODO Watch out!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            //OnDisconnect();
        } catch (Exception e) {
            System.out.println("Conexiune intrerupta. " + e.toString());
            //needs improovement
        } /*
        catch (ClassNotFoundException e) {
	    	// TODO Should I consider this a disconnect error?
			System.out.println("Object rcv error");
			e.printStackTrace();
		}*/ finally {
            try {
                socket.close();
            } catch (IOException e) {
            }
            // declansez evenimentul de conexiune terminata
            OnClientDisconnected();
        }
    }

    // eveniment de conexiune terminata
    public abstract void OnClientDisconnected();
}
