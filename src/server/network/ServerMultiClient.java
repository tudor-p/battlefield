package server.network;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

// clasa de administrare a unui serviciu de retea
// primeste mai multi clienti si paseaza efortul pe alte fire de executie
public class ServerMultiClient extends Thread implements IClientManagement {
    // lista de clienti administrati
    ArrayList<AClientThread> clients_served = new ArrayList<AClientThread>();
    // portul local pe care se asculta
    private int listening_port;
    // starea serverului
    private boolean keep_accepting;
    // fabrica de clienti
    private IClientThreadFactory client_factory = null;
    // socketul pe care asculta serverul
    private ServerSocket listening_socket = null;

    // ctor, primeste portul pe care asculta serverul si referinta la o fabrica de clienti
    public ServerMultiClient(int port, IClientThreadFactory custom_client_factory) {
        clients_served.clear();
        keep_accepting = true;
        listening_port = port;
        this.client_factory = custom_client_factory;
        start();
    }

    // metoda de inchis serverul
    public void StopAccepting() {
        keep_accepting = false;
    }

    // fir de executie pe care sunt acceptati si pasati noii clienti
    public void run() {
        try {
            listening_socket = new ServerSocket(listening_port);
            System.out.println("listening on port: " + listening_port);
        } catch (Exception e) {
            System.out.println("Port seems to be occupied!!! Change port, or free it!");
            System.out.println("Server failed to initialize" + e.toString());
            return;
        }
        System.out.println("Server Started");
        try {
            while (keep_accepting) {
                // Blocks until a connection occurs:
                Socket new_socket = listening_socket.accept();
                AClientThread new_client_thread = null;
                try {
                    new_client_thread = client_factory.getClientThread(new_socket);
                } catch (Exception e) {
                    System.out.println(e.toString());
                    e.printStackTrace();
                    // If it fails, close the socket,
                    // otherwise the thread will close it:
                    if (new_client_thread != null) {
                        new_client_thread.Close();
                    } else {
                        new_socket.close();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (keep_accepting) {
                    System.out.println("Failled accepting.");
                }
                System.out.println("Server Closed.");
                listening_socket.close();
            } catch (IOException e) {
                System.out.println("Failed to close socket...");
                e.printStackTrace();
            }
        }
    }

    // metoda pentru inchis serverul
    public void Close() {
        keep_accepting = false;
        for (AClientThread c : clients_served) {
            c.Send("exit");
            c.Close();
        }
        try {
            listening_socket.close();
        } catch (IOException e) {
            System.out.println("error closing socketServer " + e.toString());
            e.printStackTrace();
        }
    }

    // rutina pentru tratarea evenimentului de client deconectat
    @Override
    public void ClientDisconnected(AClientThread client) {
        clients_served.remove((AClientThread) client);
    }

    // rutina pentru broadcast de mesaje
    @Override
    public void Broadcast(String s) {
        for (AClientThread client : clients_served) {
            client.Send(s);
        }
    }

    // rutina pentru tratarea evenimentului de client deconectat
    @Override
    public void ClientConnected(AClientThread client) {
        clients_served.add(client);
    }
}