package server.network;

import java.net.Socket;

public interface IClientThreadFactory {
    public AClientThread getClientThread(Socket s);
}
