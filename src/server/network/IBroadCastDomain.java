package server.network;

// interfata pentru stabilirea proprietatilor de domeniu de diseminare
public interface IBroadCastDomain {
    // metoda de diseminare
    void Broadcast(String s);
}
