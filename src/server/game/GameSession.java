package server.game;

import server.map.MapSpaceEngine;
import server.map.MapTank;
import server.network.IBroadCastDomain;

import java.util.ArrayList;

public class GameSession implements IBroadCastDomain {
    //numara instantele de GameSession
    private static int id_generator = 0;
    //genereaza id unic
    public final int game_id = ++id_generator;
    //proprietarul sesiunilor de joc
    private GamesAndPlayersManager games_and_players_manager;
    //proprietarul momentan al jocului
    private Player owner;
    //ajuta la blocarea incercarilor de conectare dupa incepera jocului
    private boolean started;
    //contorizeaza numarul de jucatori pregatiti pentru inceperea jocului
    private int ready_factor;
    //gestioneaza userii conectati la jocul curent
    private ArrayList<Player> players = new ArrayList<Player>();
    //instanta de gestionare a spatiului de joc, logica jocului
    private MapSpaceEngine ms = new MapSpaceEngine(this);

    //ctor
    // owner 						userul proprietar al jocului
    // game_and_player_manager		gestionarul jocurilor
    GameSession(Player owner, GamesAndPlayersManager game_and_player_manager) {
        this.owner = owner;
        this.games_and_players_manager = game_and_player_manager;
        owner.setHost(true);
        game_and_player_manager.gameNew(this);
        players.add(owner);
        ms.getTank(owner);
    }

    // metoda de serializare
    @Override
    public String toString() {
        return new String(owner + " " + game_id);
    }

    // getter pentru proprietarul curent
    public Player getOwner() {
        return owner;
    }

    // metoda de acceptare a unui nou client
    // impiedica conectarea la doua servere simultan
    // aloca pozitiil pe harta
    public void Join(Player player) {
        if (!started && player.getGame_session() == null) {
            ms.getTank(player);
            if (player.getTank() == null) {
                //nu mai este spatiu pe harta... sry... schimb echipa
                player.setTeam(3 - player.getTeam());//change parts
                ms.getTank(player);
                if (player.getTank() == null) {
                    return;
                }
            }
            for (Player player_allready_connected : players) {
                player.Send("Joined " + player_allready_connected + " " + player_allready_connected.getTeam() + " " + game_id);
            }
            players.add(player);
            player.setGame_session(this);
            player.setHost(false);
            Broadcast("Joined " + player + " " + player.getTeam() + " " + game_id);
        }
    }

    // Raspuns la cere de quit
    // anunta quitul
    public void Quit(Player player) {
        Broadcast("gone " + player);
        if (player.isHost() && !started) {
            games_and_players_manager.gameOver(this);
        }
        if (players.size() > 1) {
            players.remove(player);
            games_and_players_manager.userGone(player);
        } else {
            players.remove(player);
            games_and_players_manager.userGone(player);
            games_and_players_manager.gameOver(this);
        }
    }

    // metoda de broadcast a informatiilor cu privire la jocul curent catre toti cei implicati
    @Override
    public void Broadcast(String s) {
        for (Player l : players) {
            l.Send(s);
        }
    }

    // metoda pentru intampinarea cererii de ready din partea clientilor jocului
    public void tryStart() {
        if (getReady_factor() == players.size()) {
            Broadcast("start");
            started = true;
            ms.Start();
        }
    }

    // anunta schimbarea echipei unui jucator
    public void teamChanged(Player l) {
        MapTank old_tank = l.getTank();
        l.setTank(null);
        ms.getTank(l);
        if (l.getTank() != null) {
            ms.remMovable(old_tank);
            ms.unmarkObject(old_tank);
            Broadcast("team " + l.id + " " + l.getTeam());
        } else {
            l.setTeam(3 - l.getTeam());
        }
    }

    // opreste gestionarul de joc
    public void Stop() {
        ms.Stop();
    }
    // interogheaza starea jocului

    /**
     * @return the started
     */
    public boolean isStarted() {
        return started;
    }
    // incrementeaza factorul de ready

    /**
     * @param increments the ready_factor
     */
    public void incReady_factor() {
        ++this.ready_factor;
    }
    // decrementeaza factorul de ready

    /**
     * @param decrements the ready_factor
     */
    public void decReady_factor() {
        --this.ready_factor;
    }
    // inspecteza factorul de ready

    /**
     * @return the ready_factor
     */
    public int getReady_factor() {
        return ready_factor;
    }
}
