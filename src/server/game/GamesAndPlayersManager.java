package server.game;

import server.network.IClientManagement;
import server.network.ServerMultiClient;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

//serviciu de administrare generala a informatiilor legate de serverul central
//clasa de administrare a listei de jocuri si jucatori
//raporteaza strea serverului fiecarui nou client
//anunta clientii conectati despre jocuri si jucatori noi
//		sau inchidere de jocuri si plecari de jucatori
public class GamesAndPlayersManager implements Runnable {
    // TODO Punctul de intrare in aplicatie
    public final static String version = new String("v1.0 beta 09.06.09 15:06");
    //"c:\Program Files\Java\jre6\bin\java.exe" -jar "c:\Users\t\Desktop\test.jar"
    //Instanta serverului de retea
    private ServerMultiClient network_server_manager;
    //lista de clienti conectati
    private ArrayList<Player> players = new ArrayList<Player>();
    //lista de jocuri curente
    private ArrayList<GameSession> games = new ArrayList<GameSession>();

    private int port = 39016;

    //ctor pentru serverul de jocuri
    //primeste parametru portul pe care va rula interfata de retea
    public GamesAndPlayersManager(String port) {
        try {
            this.port = Integer.parseInt(port);
        } catch (Exception e) {
            System.out.println("Invalid port argument!");
        }

        PlayerFactory player_factory = new PlayerFactory(this);
        network_server_manager = new ServerMultiClient(this.port, player_factory);
    }

    public static void main(String argv[]) {
        String port = "39016";
        if (argv.length != 1) {
            System.out.println("S-a utilizat portul implicit " + port);
            System.out.println("Trimiteti primul argument portul pe care sa asculte serverul!");
        } else {

        }
        GamesAndPlayersManager glm = new GamesAndPlayersManager(port);
        glm.run();
    }

    @Override
    public void run() {
        System.out.println("BattleField Server " + version);
        System.out.println("©2009 Popovici Alexandru-Tudor");

        JFrame f = new JFrame();
        JPanel p = new JPanel(new FlowLayout());
        JLabel l = new JLabel("Server is running on default port 39016.");
        JLabel jl = new JLabel("Run from console for additional options.");
        f.setContentPane(p);
        l.setPreferredSize(new Dimension(400, 40));
        jl.setPreferredSize(new Dimension(400, 40));
        p.add(l);
        p.add(jl);
        f.setSize(new Dimension(500, 200));
        f.setTitle("Battle Field server " + version + " is running.");
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        while (f.isVisible()) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
                break;
            }
        }
        Close();
        if (f.isVisible()) {
            f.setVisible(false);
        }
    }

    //getter pt serverul de retea
    public IClientManagement getRawClientManager() {
        return network_server_manager;
    }

    //genereaza stringul cu lista de useri
    private String userList() {
        String response = new String("usrLst");
        for (Player player : players) {
            response += " " + player;
        }
        return response;
    }

    //genereaza stringul cu lista de jocuri curente
    private String gameList() {
        String response = new String("gameLst");
        for (GameSession game : games) {
            response += " " + game;
        }
        return response;
    }

    //anunta si retine aparitia unui joc nou
    public void gameNew(GameSession game) {
        games.add(game);
        network_server_manager.Broadcast("gameNew " + game);
    }

    //anunta si elimina un joc terminat
    public void gameOver(GameSession game) {
        games.remove(game);
        game.Stop();
        network_server_manager.Broadcast("gameOver " + game);
    }

    //genereaza stringul pt semnalizarea unui nou jucator
    private String userNew(Player player) {
        return new String("usrNew " + player);
    }

    //raspunde la cererea de join la un joc cu un anumit gid
    public void userJoin(Player player, int game_id) {
        for (GameSession g : games) {
            if (g.game_id == game_id) {
                g.Join(player);
                return;
            }
        }
    }

    //reactioneaza la conectarea unui nou user pe serverul de retea
    //il anunta, il ia in evidenta, il informeaza in legatura cu starea serverului
    public void userConnected(Player player) {
        //TODO Aici conexiunea e stabilita! pay attention
        network_server_manager.Broadcast(userNew(player));
        players.add(player);
        player.Send(userList());
        player.Send(gameList());
    }

    //reactioneaza la deconectarea voluntara sau involuntara a unui client
    //eg: pierde conexiunea...
    public void userDisconnected(Player player) {
        if (player.isHost()) {
            //daca are joc in administrare se incearca pasarea responsabilitatii
            if (player.getGame_session() != null) {
                player.getGame_session().Quit(player);
            }
        }
        players.remove(player);
        //TODO Pay attention
        //pentru pastrarea coerentei datelor se anunta si serverul de retea
        //		pentru a lua masuri in caz de deconectare voluntara
        network_server_manager.ClientDisconnected(player);
        //se anunta clientii ramasi
        network_server_manager.Broadcast("left " + player);
    }

    //actualizeaza informatiile pentru jucatorii care parasesc un joc
    //anunta intoarcerea jucatorului in lobby
    public void userGone(Player player) {
        player.setTank(null);
        player.setReady(false);
        player.setGame_session(null);
        player.setHost(false);
        network_server_manager.Broadcast("back " + player);
    }

    //anunta actualizarea informatiilor in cazul schimbarii echipei/nickului
    public void updatePlayerInfo(Player player) {
        network_server_manager.Broadcast("nick " + player + " " + player.getTeam());
    }

    //asigura inchiderea serverului la comanda
    public void Close() {
        network_server_manager.Close();
    }
}