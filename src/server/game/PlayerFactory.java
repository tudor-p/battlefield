package server.game;

import server.network.AClientThread;
import server.network.IClientThreadFactory;

import java.net.Socket;

// Fabrica de playeri
// stabileste proprietatile comune
public class PlayerFactory implements IClientThreadFactory {
    // numele spune tot
    private GamesAndPlayersManager game_and_player_manager_to_be_inherited;

    // ctor
    // initializarea Fabricii
    public PlayerFactory(GamesAndPlayersManager glm) {
        this.game_and_player_manager_to_be_inherited = glm;
    }

    //Creeaza o noua instanta
    @Override
    public AClientThread getClientThread(Socket socket) {
        Player new_player = new Player(socket, game_and_player_manager_to_be_inherited);
        game_and_player_manager_to_be_inherited.getRawClientManager().ClientConnected(new_player);
        return new_player;
    }
}
