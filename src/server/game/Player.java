package server.game;

import server.map.MapDirection;
import server.map.MapTank;
import server.network.AClientThread;

import java.net.Socket;

public class Player extends AClientThread {
    // generator de id unic
    private static int id_generator = 0;
    // id unic de identificare rapida
    public final int id = ++id_generator;
    // nick user friendly
    private String nick;
    // id jocului la care face parte
    private int game_id;
    // are joc in administrare?
    private boolean host;
    // jocul cu care este asociat
    private GameSession game_session = null;
    // managerul de useri
    private GamesAndPlayersManager lobby_manager = null;
    // tancul asociat, primeste comenzile de miscare
    private MapTank tank = null;
    // starea de pregatit pentru inceperea jocului
    private boolean ready;
    // echipa din care face parte 1 sau 2
    private int team;

    // ctor
    // Socketul pe care comunica
    // managerul de clienti aferent
    public Player(Socket s, GamesAndPlayersManager glm) {
        super(s);
        this.lobby_manager = glm;
        setGame_id(0);
        setNick(new String("noname" + id));
        setTeam(2);
        Start();
    }

    // metoda de serializare
    @Override
    public String toString() {
        return new String(getNick() + " " + id);
    }

    // metoda de administrare a evenimentului de joc terminat
    public void GameOver() {
        getGame_session().Quit(this);
    }

    // administrarea evenimentului de conectare
    @Override
    public void OnClientConnected() {
        this.Send("connected " + this + " " + getTeam());
        lobby_manager.userConnected(this);
    }

    // administrarea evenimentului de deconectare, indiferent de cauza acestuia
    @Override
    public void OnClientDisconnected() {
        lobby_manager.userDisconnected(this);
    }

    // metoda responsabila pentru primirea si interpretarea mesajelor pe retea
    @Override
    public void Recv(String str) {
        if (str.startsWith("nick")) {
            if (game_session != null) {
                if (game_session.isStarted()) {
                    return;
                }
            }
            try {
                String strs[] = str.split(" ");
                nick = strs[1].trim();
                int nt = Integer.parseInt(strs[2].trim());
                if (getGame_session() != null && nt != getTeam()) {
                    setTeam(nt);
                    game_session.teamChanged(this);
                } else {
                    setTeam(nt);
                }
                lobby_manager.updatePlayerInfo(this);
            } catch (Exception e) {
                System.out.println("Bad nick change attempt");
                System.out.println(e.toString());
            }
            return;
        }
        if (str.startsWith("create")) {
            if (getGame_session() == null) {
                setGame_session(new GameSession(this, lobby_manager));
            }
            return;
        }
        if (str.startsWith("join")) {
            if (game_session == null) {
                int gid = 0;
                try {
                    //gid = Integer.parseInt(str.split("\"")[2].trim());
                    gid = Integer.parseInt(str.split(" ")[1].trim());
                } catch (Exception e) {
                    return;
                }
                lobby_manager.userJoin(this, gid);
            }
            return;
        }
        if (str.startsWith("start")) {
            if (getGame_session() != null && !this.isReady()) {
                setReady(true);
                getGame_session().incReady_factor();
                getGame_session().tryStart();
            }
        }
        if (str.startsWith("cancel")) {
            if (getGame_session() != null && this.isReady()) {
                setReady(false);
                getGame_session().decReady_factor();
            }
        }
        if (str.startsWith("quit")) {
            if (getGame_session() != null) {
                if (game_session.isStarted()) {
                    tank.Collision(null);
                } else {
                    game_session.Quit(this);
                }
                System.out.println("Quited " + getNick());
            }
        }
        if (str.startsWith("exit")) {
            if (getGame_session() != null) {
                getGame_session().Quit(this);
            }
            System.out.println("Exists " + getNick());
            running = false;
            //glm.userDisconnected(this);
        }
        if (str.startsWith("fire")) {
            if (tank != null) {
                tank.startFire();
            }
            return;
        }
        if (str.startsWith("efire")) {
            if (tank != null) {
                tank.stopFire();
            }
            return;
        }
        if (str.startsWith("stop")) {
            if (tank != null) {
                tank.setMoving(false);
            }
            return;
        }
        if (str.startsWith("team")) {
            if (game_session != null) {
                if (game_session.isStarted()) {
                    return;
                }
            }
            int nt = Integer.parseInt(str.split(" ")[1].trim());
            if (getGame_session() != null && nt != team) {
                setTeam(nt);
                getGame_session().teamChanged(this);
            } else {
                setTeam(nt);
            }
            return;
        }
        try {
            if (tank != null) {
                tank.setDir(MapDirection.valueOf(str));
                //cand se sesizeaza schimbarea de directie, la prima iteratie se schimba directia
                //la a doua se deplaseaza, pt rotatie pe loc
                tank.setMoving(true);
            }
        } catch (Exception e) {
            //System.out.println(str + "\n"+e.toString());
        }
    }

    /**
     * @return the game_session
     */
    public GameSession getGame_session() {
        return game_session;
    }

    /**
     * @param game_session the game_session to set
     */
    public void setGame_session(GameSession game_session) {
        this.game_session = game_session;
    }

    /**
     * @return the game_id
     */
    public int getGame_id() {
        return game_id;
    }

    /**
     * @param game_id the game_id to set
     */
    public void setGame_id(int game_id) {
        this.game_id = game_id;
    }

    /**
     * @return the nick
     */
    public String getNick() {
        return nick;
    }

    /**
     * @param nick the nick to set
     */
    public void setNick(String nick) {
        this.nick = nick;
    }

    /**
     * @return the ready
     */
    public boolean isReady() {
        return ready;
    }

    /**
     * @param ready the ready to set
     */
    public void setReady(boolean ready) {
        this.ready = ready;
    }

    /**
     * @return the team
     */
    public int getTeam() {
        return team;
    }

    /**
     * @param team the team to set
     */
    public void setTeam(int team) {
        if (team != 1 && team != 2) {
            return;
        }
        this.team = team;
    }

    /**
     * @return the host
     */
    public boolean isHost() {
        return host;
    }

    /**
     * @param host the host to set
     */
    void setHost(boolean host) {
        this.host = host;
    }

    public MapTank getTank() {
        return tank;
    }

    public void setTank(MapTank mapTank) {
        tank = mapTank;
    }
}
