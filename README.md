# Battle Field

## Contextul initial al proiectului

Proiect de disciplina pentru Paradigme de Programare (2009) care sa foloseasca limbajul Java si concepte de design patterns.

### Tema
Reproducerea in Java a jocului [Battle City](https://en.wikipedia.org/wiki/Battle_City_(video_game)) (joc video [single player](https://www.youtube.com/watch?v=MPsA5PtfdL0)/[dual player](https://www.youtube.com/watch?v=g3aTE-Cvaj4)
din anii 1980 pentru consola NES ([Nintendo Entertainment System](https://en.wikipedia.org/wiki/Nintendo_Entertainment_System)) adaptare a consolei japoneze Famicom (Family Computer).
O clona a acestei console ([Terminator 2](https://en.wikipedia.org/wiki/Ending-Man_Terminator)) a fost comercializata incepand cu sfarsitul anilor 1990 in Europa de Est, ajungand si in Romania.

### Idee
Reimplementare partiala a jocului original, dar in format multiplayer, pentru retea sau internet, care sa permita jocul intre doua echipe.

Serverul permite conectarea clientilor, administrarea unui Lobby (creare jocuri de catre oricere dintre clienti, listarea jocurilor create, alaturarea la un joc creat).

### Implementarea initiala
Realizat in colaborare de Tudor Popovici si Bogdan Balaita ca doua aplicatii separate:
  - Server (pachetul curent server, TP)
  - Client (pachetul curent client, BB)

### Proiectul actual
Cele doua proiecte originale realizate in IDE-ul Eclipse au fost grupate intr-un singur proiect pentru IDE-ul [IntelliJ Community Edition](https://idea-intellij.com/intellij-community/), continuturile originale fiind usor refactorizate si separate in pachetele radacina server si client.

A fost adaugata clasa MainWindow in radacina care deriva din clasa client.gameplay.JIntroFrame (clientul original) care da posibilitatea de lansare si a aplicatiei server (server.game.GamesAndPLayersManager).

A fost configurata producerea la build a fisierului BattleField.jar care lanseaza clasa integranta MainWindow.

### Build si Rulare
Pentru build este recomandat mediul Intellij (cu un mic efort proiectul poate fi adaptat si pentru alte medii).

Cu Intellij se genereaza automat in directorul proiectului artefactul executabil BattleField.jar care la rulare afiseaza fereastra client.

Macar unul dintre jucatori trebuie sa porneasca serverul, iar apoi el si ceilalti jucatori se vor putea conecta la acel server.

Este recomandata schimbarea nick-ului implicit inaintea conectarii la server.
Pentru a juca este necesara crearea cel putin a unui joc si eventual alaturarea altor jucatori.

Utilizatorul serverului decide pornirea jocului, iar jucatorii conectati confirma fiecare, iar cand toti au confirmat jocul porneste.
La pornire este posibil sa apara un timp mort in care serverul comunica elementele hartii fiecarui jucator.

Este posibil sa fie necesara autorizarea aplicatiei pentru a putea trece de firewall.
Clientii serverului trebuie sa cunoasca adresa IP din retea/internet a serverului.

### Disclaimer
Aceasta aplicatie nu vine cu nici un fel de garantii relativ la functionarea corecta sau sigura.
Utilizarea sau modificarea ei este o confirmare a acordului ca utilizatorul isi asuma toata raspunderea pentru riscurile sau daunele aparute.

## Specificatiile originale ale proiectului
In aceasta sectiune este reprodus setul de specificatii esentiale ale protocolului de comunicatie in retea in baza caruia implementarea serverului si a clientului s-a realizat in paralel de catre cei doi membri ai echipei de proiect.

### Legenda terminologie
```
[ceva] descrie semnificatia valorii
<ceva> face referire la o forma serializata
{<ceva> [ceva]} descrie forma generica a unei liste, indica posibiliatatea repetitie structurii
(info suplim) da informatii suplimentare despre ultima valoare (nu apare in mesaj)
; comentariu
```
Id-uri echipe disponibile: 1/2

#### Forme serializate

```
<player>=[nick] [id_numeric]
<game>  =<player>(proprietrul curent) [id_numeric_joc]
<coord> =[x] [y]
<obj>	=[MapXXXXX] [id_numeric_obiect] <coord>(pozitie) [dir] ...(info aditionale)
```

#### La conectare clientul primeste:
```
connected [id_num_player]
```
si dupa vin restul:

```
usrLst {<player>}
gameLst {<game>}
```

#### Pachete primite de la server in lobby

```
gameNew <game>  ; un joc nou
gameOver <game> ; un joc terminat
usrNew <player> ; un jucator nou in lobby
left <player>   ; un jucator a parasit lobby-ul
nick <player> [team] ;
```

#### Pachete primite de client in timpul jocului

```
joined <player> [team] [game_id]
gone <player>
team [player_id] [new_team]
start
paint <obj>
paint MapTank [id] <coord> [dir] [owner_id] [team]
erase <obj>
ping					;comanda sincronizare/redesenarea
```

#### Cereri de la client
```
nick [nick](fara spatii) [team] 	;ignorat dupa ce a inceput jocul
team [team]				;permite alegerea echipei
create					;inceraca asocierea unui nou joc
join [game_id]				;ignorat dupa create sau join
start					;anunta servarul ca e pregatit sa inceapa
cancel                 			;revine asupra msg de start
quit					;iese din jocul curent, daca este cazul
exit					;cere deconectarea de la server, trebuie legat de inchiderea aplicatiei
```

##### Cereri de miscare din partea clientului:
```
North
South
East
West
stop
fire ;start fire
efire ;end fire
```

### Plan realizare diagrame:
- Clase
- Usecase
- Deployment
- Module

## Todos
 - Refactor implementare
 - Scriere teste de unitate
 - Scriere teste de integrare

## License
----
GPLv2

